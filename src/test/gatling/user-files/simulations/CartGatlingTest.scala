import _root_.io.gatling.core.scenario.Simulation
import ch.qos.logback.classic.{Level, LoggerContext}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import org.slf4j.LoggerFactory

import scala.concurrent.duration._

/**
 * Performance test for the OrderLine entity.
 */
class CartGatlingTest extends Simulation {

    val context: LoggerContext = LoggerFactory.getILoggerFactory.asInstanceOf[LoggerContext]
    // Log all HTTP requests
    //context.getLogger("io.gatling.http").setLevel(Level.valueOf("TRACE"))
    // Log failed HTTP requests
    //context.getLogger("io.gatling.http").setLevel(Level.valueOf("DEBUG"))

    val baseURL = Option(System.getProperty("baseURL")) getOrElse """http://localhost:8080"""

    val httpConf = http
        .baseUrl(baseURL)
        .inferHtmlResources()
        .acceptHeader("*/*")
        .acceptEncodingHeader("gzip, deflate")
        .acceptLanguageHeader("fr,fr-fr;q=0.8,en-us;q=0.5,en;q=0.3")
        .connectionHeader("keep-alive")
        .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:33.0) Gecko/20100101 Firefox/33.0")
        .silentResources // Silence all resources like css or css so they don't clutter the results

    val headers_http = Map(
        "Accept" -> """application/json"""
    )

    val headers_http_authentication = Map(
        "Content-Type" -> """application/json""",
        "Accept" -> """application/json"""
    )

    val headers_http_authenticated = Map(
        "Accept" -> """application/json""",
        "Authorization" -> "${access_token}"
    )

    val scn = scenario("Add to cart 1")
        .exec(http("First unauthenticated request")
        .get("/api/account")
        .headers(headers_http)
        .check(status.is(401))
        ).exitHereIfFailed
        .pause(10)
        .exec(http("Authentication")
        .post("/api/authenticate")
        .headers(headers_http_authentication)
        .body(StringBody("""{"username":"user", "password":"user"}""")).asJson
        .check(header("Authorization").saveAs("access_token"))).exitHereIfFailed
        .pause(2)
        .exec(http("Authenticated request")
        .get("/api/account")
        .headers(headers_http_authenticated)
        .check(status.is(200)))
        .pause(5)
        .repeat(2) {
            exec(http("Add to Cart").put("/api/cart")
                .headers(headers_http_authenticated)
                .body(StringBody("{\n  \"id\" : 1,\n  \"quantity\" : 1,\n  \"product\" : {\n    \"id\" : 1,\n    \"name\" : \"Generic Steel Tuna\",\n    \"quantity\" : 53676,\n    \"price\" : 31485.00,\n    \"description\" : \"calculating South Dakota monetize\",\n    \"imagePath\" : \"Consultant virtual wireless\"\n  },\n  \"productOrder\" : {\n    \"id\" : 1,\n    \"creationDate\" : \"2020-09-06T02:29:10Z\",\n    \"status\" : \"CART\",\n    \"customer\" : {\n      \"id\" : 3,\n      \"gender\" : \"MALE\",\n      \"birthDate\" : \"2020-09-06T07:35:00Z\",\n      \"user\" : {\n        \"id\" : 4,\n        \"login\" : \"user\",\n        \"firstName\" : \"User\",\n        \"lastName\" : \"User\",\n        \"email\" : \"user@localhost\",\n        \"activated\" : true,\n        \"langKey\" : \"fr\",\n        \"imageUrl\" : \"\",\n        \"resetDate\" : null\n      }\n    }\n  }\n}"))
                .asJson.check(status.is(200)))
            }

    val scn2 = scenario("Add To Cart 2")
        .exec(http("First unauthenticated request")
            .get("/api/account")
            .headers(headers_http)
            .check(status.is(401))
        ).exitHereIfFailed
        .pause(10)
        .exec(http("Authentication")
            .post("/api/authenticate")
            .headers(headers_http_authentication)
            .body(StringBody("""{"username":"user", "password":"user"}""")).asJson
            .check(header("Authorization").saveAs("access_token"))).exitHereIfFailed
        .pause(2)
        .exec(http("Authenticated request")
            .get("/api/account")
            .headers(headers_http_authenticated)
            .check(status.is(200)))
        .pause(5)
        .repeat(2) {
            exec(http("Add to Cart").put("/api/cart")
                .headers(headers_http_authenticated)
                .body(StringBody("{\n  \"id\" : 2,\n  \"quantity\" : 1,\n  \"product\" : {\n    \"id\" : 2,\n    \"name\" : \"Generic Steel Tuna\",\n    \"quantity\" : 53676,\n    \"price\" : 31485.00,\n    \"description\" : \"calculating South Dakota monetize\",\n    \"imagePath\" : \"Consultant virtual wireless\"\n  },\n  \"productOrder\" : {\n    \"id\" : 1,\n    \"creationDate\" : \"2020-09-06T02:29:10Z\",\n    \"status\" : \"CART\",\n    \"customer\" : {\n      \"id\" : 3,\n      \"gender\" : \"MALE\",\n      \"birthDate\" : \"2020-09-06T07:35:00Z\",\n      \"user\" : {\n        \"id\" : 4,\n        \"login\" : \"user\",\n        \"firstName\" : \"User\",\n        \"lastName\" : \"User\",\n        \"email\" : \"user@localhost\",\n        \"activated\" : true,\n        \"langKey\" : \"fr\",\n        \"imageUrl\" : \"\",\n        \"resetDate\" : null\n      }\n    }\n  }\n}"))
                .asJson.check(status.is(200)))
        }


    val scn3 = scenario("Change the stock as admin")
        .exec(http("First unauthenticated request")
            .get("/api/account")
            .headers(headers_http)
            .check(status.is(401))
        ).exitHereIfFailed
        .pause(10)
        .exec(http("Authentication")
            .post("/api/authenticate")
            .headers(headers_http_authentication)
            .body(StringBody("""{"username":"admin", "password":"admin"}""")).asJson
            .check(header("Authorization").saveAs("access_token"))).exitHereIfFailed
        .pause(2)
        .exec(http("Authenticated request")
            .get("/api/account")
            .headers(headers_http_authenticated)
            .check(status.is(200)))
        .pause(5)
        .repeat(2, "index") {
            exec({ session =>
                val index = session("index").as[Int]
                val id = index+1000
                session.set("id", id)
            }).exec(http("Modifying the product").put("/api/products")
                .headers(headers_http_authenticated)
                .body(StringBody("{\n  \"id\" : 1,\n  \"name\" : \"Generic Steel Tuna\",\n  \"stockQuantity\" : "+"${id}"+",\n  \"price\" : 31485,\n  \"description\" : \"calculating South Dakota monetize\",\n  \"imagePath\" : \"Consultant virtual wireless\"\n}"))
                .asJson.check(status.is(200)))
        }


    val cart = scenario("add To Cart").exec(scn);
    val cart2 = scenario("add To Cart 2").exec(scn2);
    val modifyProduct = scenario("modifying the product").exec(scn3);




    setUp(
        cart.inject(rampUsers(Integer.getInteger("users", 100)) during (Integer.getInteger("ramp", 1) minutes)).protocols(httpConf),
        cart2.inject(rampUsers(Integer.getInteger("users", 100)) during (Integer.getInteger("ramp", 1) minutes)).protocols(httpConf),
        modifyProduct.inject(rampUsers(Integer.getInteger("users", 100)) during (Integer.getInteger("ramp", 1) minutes)).protocols(httpConf)
    )
}

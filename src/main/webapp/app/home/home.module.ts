import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SpaghettomatosSharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { SearchFilterComponent } from 'app/home/search-filter.component';
import { MatSidenavModule } from '@angular/material/sidenav';

@NgModule({
  imports: [
    SpaghettomatosSharedModule,
    RouterModule.forChild([HOME_ROUTE]),
    MatPaginatorModule,
    MatTableModule,
    MatSortModule,
    MatSidenavModule,
  ],
  declarations: [HomeComponent, SearchFilterComponent],
})
export class SpaghettomatosHomeModule {}

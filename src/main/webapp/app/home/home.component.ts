import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';
import { MatTableDataSource } from '@angular/material/table';
import { IProduct } from 'app/shared/model/product.model';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { IOrderLine, OrderLine } from 'app/shared/model/order-line.model';
import { CartService } from 'app/entities/cart/cart.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { HomeService } from 'app/home/home.service';
import { Category, ICategory } from 'app/shared/model/category.model';
import { isBoolean } from 'util';
import { SignInNotificationService } from 'app/shared/signInNotification/signInNotificationService';
import { CartContentNotificationService } from 'app/shared/cartContentNotification/cartContentNotificationService';
import { MatSnackBar } from '@angular/material/snack-bar';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: ['home.scss'],
})
export class HomeComponent implements OnInit, OnDestroy, AfterViewInit {
  categoryTree: ICategory | null = null;
  account: Account | null = null;
  authSubscription?: Subscription;
  numIndexCategory: number;
  categoryDisplayName: string[] = [];

  displayedColumns: string[] = ['image', 'name', 'stockQuantity', 'price', 'addCartButton'];
  dataSource: MatTableDataSource<IProduct> = new MatTableDataSource<IProduct>([]);

  paginatorLength = 0;

  paginator?: MatPaginator = undefined;

  constructor(
    protected activatedRoute: ActivatedRoute,
    public accountService: AccountService,
    protected productService: HomeService,
    private cartService: CartService,
    private router: Router,
    private signInNotificationService: SignInNotificationService,
    private cartContentNotificationService: CartContentNotificationService,
    private snackBar: MatSnackBar
  ) {
    this.numIndexCategory = 0;
  }

  ngAfterViewInit(): void {
    this.updatePage();
    this.paginator?.page.pipe(tap(() => this.updatePage())).subscribe();
  }

  @ViewChild(MatPaginator) set matPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
  }

  @ViewChild(MatSort) set matSort(sort: MatSort) {
    this.dataSource.sort = sort;
  }

  ngOnInit(): void {
    this.authSubscription = this.accountService.getAuthenticationState().subscribe(account => (this.account = account));
    this.activatedRoute.data.subscribe(({ categoryTree }) => {
      this.categoryTree = categoryTree;
    });
  }

  updatePage(): void {
    if (this.categoryDisplayName != null && this.categoryDisplayName.length !== 0) {
      this.updatePageWithFilter();
    } else {
      this.productService
        .query({
          page: this.paginator?.pageIndex,
          size: this.paginator?.pageSize,
        })
        .subscribe(res => {
          this.dataSource.data = res.body || [];
          this.paginatorLength = parseInt(res.headers.get('x-total-count') || '0', 10);
        });
    }
  }

  updatePageWithFilter(): void {
    this.productService
      .findAllCategoriesFilter(this.categoryDisplayName.join(','), {
        page: this.paginator?.pageIndex,
        size: this.paginator?.pageSize,
      })
      .subscribe(res => {
        this.dataSource.data = res.body || [];
        this.paginatorLength = parseInt(res.headers.get('x-total-count') || '0', 10);
      });
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  openSignInMenu(): void {
    this.signInNotificationService.open();
  }

  updateNavBarBadge(): void {
    this.cartContentNotificationService.update();
  }

  login(): void {}

  ngOnDestroy(): void {
    if (this.authSubscription) {
      this.authSubscription.unsubscribe();
    }
  }

  openSuccessSnackBar(message: string): void {
    this.snackBar.open(message, 'x', { duration: 5000, panelClass: ['spaghettsuccess', 'snack-bar'], verticalPosition: 'top' });
  }

  openErrorSnackBar(message: string): void {
    this.snackBar.open(message, 'x', { duration: 5000, panelClass: ['spaghetterror', 'snack-bar'], verticalPosition: 'top' });
  }

  addToCart(product: IProduct): void {
    if (this.accountService.hasAnyAuthority('ROLE_USER')) {
      this.cartService.getOrderLine(product.id!).subscribe((res: HttpResponse<IOrderLine>) => {
        let orderLine = res.body || undefined;
        if (!orderLine) {
          orderLine = new OrderLine(undefined, 1, product, undefined);
          this.cartService.addToCart(orderLine).subscribe(() => {
            this.router.navigate(['/']);
            this.updateNavBarBadge();
            this.openSuccessSnackBar(orderLine?.product?.name + ' à bien été ajouté à votre panier.');
          });
        } else {
          orderLine.quantity! += 1;
          this.cartService.modifyFromCart(orderLine).subscribe(() => {
            this.router.navigate(['/']);
            this.updateNavBarBadge();
            this.openSuccessSnackBar(orderLine?.product?.name + ' à bien été ajouté à votre panier.');
          });
        }
      });
    } else if (!this.accountService.hasAnyAuthority('ROLE_ADMIN')) {
      this.openSignInMenu();
      this.openErrorSnackBar('Vous devez être connecté pour commander un article.');
    }
  }

  processReq(categoryDown: ICategory): void {
    if (this.paginator !== undefined && this.paginator.pageIndex != null) {
      this.paginator.pageIndex = 0;
    }
    this.numIndexCategory = 0;
    this.categoryDisplayName = [];
    this.fillCategory(categoryDown, true);
    this.updatePage();
  }

  fillCategory(categoryDown: ICategory, categoryRoot: boolean): void {
    let addToCategoryDisplay;
    if (categoryRoot || (categoryDown !== undefined && categoryDown.id !== -1)) {
      if (!categoryRoot) {
        addToCategoryDisplay = true;
        if (categoryDown.children != null && categoryDown.children.length !== 0) {
          for (let i = 0; i < categoryDown.children.length; i++) {
            if (categoryDown.children[i] != null) {
              addToCategoryDisplay = false;
              break;
            }
          }
        }
        if (addToCategoryDisplay && categoryDown.name != null) {
          this.categoryDisplayName[this.numIndexCategory++] = categoryDown.id + '';
        }
      }
      if (categoryDown.children != null) {
        for (let i = 0; i < categoryDown.children.length; i++) {
          if (categoryDown.children[i] !== null && categoryDown.children[i] !== undefined) {
            this.fillCategory(categoryDown.children[i], false);
          }
        }
      }
    }
  }
}

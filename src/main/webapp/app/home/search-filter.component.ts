import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Category, ICategory } from '../shared/model/category.model';

@Component({
  selector: 'jhi-search-filter',
  template: `
    <div *ngIf="category">
      <div *ngIf="category.name">
        <mat-checkbox #checkbox (change)="changeCheckbox(checkbox.checked)">{{ category.name }}</mat-checkbox>
      </div>
      <div *ngIf="isCheck">
        <div *ngIf="category.children">
          <div *ngFor="let child of category.children; let i = index">
            <div id="display-child" *ngIf="child">
              <jhi-search-filter [category]="child" (sendRequestToData)="processReq($event, i)"></jhi-search-filter>
            </div>
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./search-filter.scss'],
})
export class SearchFilterComponent implements OnInit {
  @Input() category: ICategory | null = null;
  @Input() isRoot: boolean | null = false;
  @Output() sendRequestToData = new EventEmitter();
  isCheck: boolean | null = false;
  categoryInfo: ICategory | null = null;
  indexListChecked: number;

  constructor() {
    this.indexListChecked = 0;
  }

  ngOnInit(): void {
    if (this.isRoot) {
      this.isCheck = true;
    }
  }

  changeCheckbox(isCheck: boolean): void {
    this.isCheck = isCheck;
    if (isCheck) {
      if (this.category != null) {
        this.categoryInfo = new Category(this.category.id, this.category.name, '', [], [], new Category());
      }
    } else {
      this.indexListChecked = 0;
      this.categoryInfo = null;
    }
    this.sendRequestToData.emit(this.categoryInfo);
  }

  processReq(categoryDown: ICategory, numChild: number): void {
    if (this.isRoot) {
      if (this.categoryInfo == null) {
        this.categoryInfo = new Category(-1, 'pp', '', [], [], new Category());
      }
      if (this.categoryInfo.children != null) {
        this.categoryInfo.children[numChild] = categoryDown;
        this.sendRequestToData.emit(this.categoryInfo);
      }
    } else {
      if (this.categoryInfo != null && this.categoryInfo.children != null) {
        this.categoryInfo.children[numChild] = categoryDown;
        this.sendRequestToData.emit(this.categoryInfo);
      }
    }
  }
}

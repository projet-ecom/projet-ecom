import { ActivatedRouteSnapshot, Resolve, Route, Router } from '@angular/router';

import { HomeComponent } from './home.component';
import { Injectable } from '@angular/core';
import { Category, ICategory } from 'app/shared/model/category.model';
import { CategoryService } from 'app/entities/category/category.service';
import { EMPTY, Observable, of } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { ProductResolve } from 'app/entities/product/product.route';

@Injectable({ providedIn: 'root' })
export class CategoryTreeResolve implements Resolve<ICategory> {
  constructor(private service: CategoryService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICategory> | Observable<never> {
    return this.service.findTreeCategory().pipe(
      flatMap((category: HttpResponse<Category>) => {
        if (category.body) {
          return of(category.body);
        } else {
          this.router.navigate(['404']);
          return EMPTY;
        }
      })
    );
  }
}

export const HOME_ROUTE: Route = {
  path: '',
  component: HomeComponent,
  resolve: {
    categoryTree: CategoryTreeResolve,
  },
  data: {
    authorities: [],
    pageTitle: 'home.title',
  },
};

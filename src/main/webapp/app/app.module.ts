import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { SpaghettomatosSharedModule } from 'app/shared/shared.module';
import { SpaghettomatosCoreModule } from 'app/core/core.module';
import { SpaghettomatosAppRoutingModule } from './app-routing.module';
import { SpaghettomatosHomeModule } from './home/home.module';
import { SpaghettomatosEntityModule } from './entities/entity.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatBadgeModule } from '@angular/material/badge';

@NgModule({
  imports: [
    BrowserModule,
    SpaghettomatosSharedModule,
    SpaghettomatosCoreModule,
    SpaghettomatosHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    SpaghettomatosEntityModule,
    SpaghettomatosAppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatBadgeModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  bootstrap: [MainComponent],
  exports: [BrowserAnimationsModule],
})
export class SpaghettomatosAppModule {}

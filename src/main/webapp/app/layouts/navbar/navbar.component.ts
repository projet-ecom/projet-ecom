import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { SessionStorageService } from 'ngx-webstorage';

import { VERSION } from 'app/app.constants';
import { LANGUAGES } from 'app/core/language/language.constants';
import { AccountService } from 'app/core/auth/account.service';
import { LoginService } from 'app/core/login/login.service';
import { ProfileService } from 'app/layouts/profiles/profile.service';
import { FormBuilder } from '@angular/forms';
import { MatMenuTrigger } from '@angular/material/menu';
import { SignInNotificationService } from 'app/shared/signInNotification/signInNotificationService';
import { CartContentNotificationService } from 'app/shared/cartContentNotification/cartContentNotificationService';
import { CartService } from 'app/entities/cart/cart.service';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

@Component({
  selector: 'jhi-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['navbar.scss'],
})
export class NavbarComponent implements OnInit {
  inProduction?: boolean;
  isNavbarCollapsed = true;
  languages = LANGUAGES;
  swaggerEnabled?: boolean;
  version: string;

  isOpen = false;

  @ViewChild('username', { static: false })
  username?: ElementRef;

  @ViewChild('signInMenuTrigger')
  matMenuTrigger?: MatMenuTrigger;

  cartSize = 0;

  loginForm = this.formBuilder.group({
    username: [''],
    password: [''],
    rememberMe: [false],
  });

  constructor(
    private loginService: LoginService,
    private languageService: JhiLanguageService,
    private sessionStorage: SessionStorageService,
    private accountService: AccountService,
    private profileService: ProfileService,
    private router: Router,
    private formBuilder: FormBuilder,
    private signInNotificationService: SignInNotificationService,
    private cartContentNotificationService: CartContentNotificationService,
    private cartService: CartService,
    private snackBar: MatSnackBar
  ) {
    this.version = VERSION ? (VERSION.toLowerCase().startsWith('v') ? VERSION : 'v' + VERSION) : '';
    this.signInNotificationService.menuEmitter.subscribe(this.toggleSignInMenu.bind(this));
    this.cartContentNotificationService.cartValueEmitter.subscribe(this.updateCartSize.bind(this));
  }

  ngOnInit(): void {
    this.profileService.getProfileInfo().subscribe(profileInfo => {
      this.inProduction = profileInfo.inProduction;
      this.swaggerEnabled = profileInfo.swaggerEnabled;
    });
    this.updateCartSize();
    const config = new MatSnackBarConfig();
    config.panelClass = ['custom-class'];
  }

  changeLanguage(languageKey: string): void {
    this.sessionStorage.store('locale', languageKey);
    this.languageService.changeLanguage(languageKey);
  }

  collapseNavbar(): void {
    this.isNavbarCollapsed = true;
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  logout(): void {
    this.collapseNavbar();
    this.loginService.logout();
    this.router.navigate(['']);
    this.openSuccessSnackBar('Déconnexion réussie.');
  }

  toggleNavbar(): void {
    this.isNavbarCollapsed = !this.isNavbarCollapsed;
  }

  getImageUrl(): string {
    return this.isAuthenticated() ? this.accountService.getImageUrl() : '';
  }

  loginf(): void {
    this.loginService
      .login({
        username: this.loginForm.get('username')!.value,
        password: this.loginForm.get('password')!.value,
        rememberMe: this.loginForm.get('rememberMe')!.value,
      })
      .subscribe(
        () => {
          if (
            this.router.url === '/account/register' ||
            this.router.url.startsWith('/account/activate') ||
            this.router.url.startsWith('/account/reset/')
          ) {
            this.router.navigate(['']);
          }
          this.openSuccessSnackBar('Vous êtes connecté');
        },
        () => this.openErrorSnackBar('La connection à échouée! verifiez vos identifiants.')
      );
  }

  register(): void {
    this.router.navigate(['/account/register']);
  }

  menuOpened(): void {
    this.loginForm.reset();
  }

  toggleSignInMenu(state: boolean): void {
    if (state) {
      this.matMenuTrigger?.openMenu();
    }
  }

  updateCartSize(): void {
    this.cartService.getCart().subscribe(value => {
      this.cartSize = 0;
      value.body?.orderLines?.forEach(element => (this.cartSize += element.quantity || 0));
    });
  }

  openSuccessSnackBar(message: string): void {
    this.snackBar.open(message, 'x', { duration: 5000, panelClass: ['spaghettsuccess', 'snack-bar'], verticalPosition: 'top' });
  }

  openErrorSnackBar(message: string): void {
    this.snackBar.open(message, 'x', { duration: 5000, panelClass: ['spaghetterror', 'snack-bar'], verticalPosition: 'top' });
  }

  requestResetPassword(): void {
    this.router.navigate(['/account/reset', 'request']);
  }
}

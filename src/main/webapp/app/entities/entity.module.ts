import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'customer',
        loadChildren: () => import('./customer/customer.module').then(m => m.SpaghettomatosCustomerModule),
      },
      {
        path: 'product-order',
        loadChildren: () => import('./product-order/product-order.module').then(m => m.SpaghettomatosProductOrderModule),
      },
      {
        path: 'order-line',
        loadChildren: () => import('./order-line/order-line.module').then(m => m.SpaghettomatosOrderLineModule),
      },
      {
        path: 'product',
        loadChildren: () => import('./product/product.module').then(m => m.SpaghettomatosProductModule),
      },
      {
        path: 'category',
        loadChildren: () => import('./category/category.module').then(m => m.SpaghettomatosCategoryModule),
      },
      {
        path: 'create-product',
        loadChildren: () => import('./create-product/create-product.module').then(m => m.SpaghettomatosCreateProductModule),
      },
      {
        path: 'cart',
        loadChildren: () => import('./cart/cart.module').then(m => m.SpaghettomatosCartModule),
      },
      {
        path: 'order-history',
        loadChildren: () => import('./order-history/order-history.module').then(m => m.SpaghettomatosOrderHistoryModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class SpaghettomatosEntityModule {}

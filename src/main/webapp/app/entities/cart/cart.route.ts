import { Routes } from '@angular/router';

import { CartComponent } from './cart.component';
import { UserRouteAccessService } from '../../core/auth/user-route-access-service';
import { Authority } from '../../shared/constants/authority.constants';
import { CartConfirmationComponent } from './cart-confirmation.component';
import { CartPaymentComponent } from './cart-payment.component';

export const CART_ROUTES: Routes = [
  {
    path: '',
    component: CartComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'spaghettomatosApp.cart.title',
      canActivate: [UserRouteAccessService],
    },
  },
  {
    path: 'payment',
    component: CartPaymentComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'spaghettomatosApp.cart.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'confirm',
    component: CartConfirmationComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'spaghettomatosApp.cart.title',
    },
    canActivate: [UserRouteAccessService],
  },
];

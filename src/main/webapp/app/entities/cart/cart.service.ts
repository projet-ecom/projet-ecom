import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { SERVER_API_URL } from 'app/app.constants';
import { Observable } from 'rxjs';
import { IProductOrder } from '../../shared/model/product-order.model';
import { IOrderLine } from '../../shared/model/order-line.model';

type EntityResponseType = HttpResponse<IProductOrder>;

@Injectable({ providedIn: 'root' })
export class CartService {
  public resourceUrl = SERVER_API_URL + 'api/cart';

  constructor(protected http: HttpClient) {}

  getCart(): Observable<EntityResponseType> {
    return this.http.get<IProductOrder>(this.resourceUrl, { observe: 'response' });
  }

  getOrderLine(id: number): Observable<HttpResponse<IOrderLine>> {
    return this.http.get<IOrderLine>(`${this.resourceUrl}/lineofproduct/${id}`, { observe: 'response' });
  }

  addToCart(orderLine: IOrderLine): Observable<EntityResponseType> {
    return this.http.post<IOrderLine>(this.resourceUrl, orderLine, { observe: 'response' });
  }

  removeFromCart(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  confirmCart(cart: IProductOrder): Observable<EntityResponseType> {
    return this.http.put<IProductOrder>(`${this.resourceUrl}/confirm`, cart, { observe: 'response' });
  }

  modifyFromCart(orderLine: IOrderLine): Observable<EntityResponseType> {
    return this.http.put<IOrderLine>(this.resourceUrl, orderLine, { observe: 'response' });
  }
}

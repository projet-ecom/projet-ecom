import { Component, OnInit } from '@angular/core';
import { CartService } from './cart.service';
import { Router } from '@angular/router';
import { IProductOrder } from '../../shared/model/product-order.model';
import { HttpResponse } from '@angular/common/http';
import { CartContentNotificationService } from '../../shared/cartContentNotification/cartContentNotificationService';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'jhi-cart-payment',
  templateUrl: './cart-payment.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartPaymentComponent implements OnInit {
  cart?: IProductOrder;
  requestFinished = false;
  sum = 0;
  ccForm = this.fb.group({
    ccNumber: ['', [Validators.required, Validators.minLength(16), Validators.maxLength(16)]],
    ccDate: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(5)]],
    ccv: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(3)]],
  });

  constructor(
    protected cartService: CartService,
    protected router: Router,
    protected cartContentNotificationService: CartContentNotificationService,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.cartService.getCart().subscribe((res: HttpResponse<IProductOrder>) => {
      this.cart = res.body || undefined;
      this.sum = this.totalSum(this.cart!);
      this.requestFinished = true;
    });
  }

  totalSum(po: IProductOrder): number {
    let result = 0;
    if (po) {
      for (const ol of po.orderLines!) {
        result += ol.product!.price! * ol.quantity!;
      }
    }
    return result;
  }

  confirmCart(): void {
    if (this.cart != null && this.ccForm.valid) {
      this.cartService.confirmCart(this.cart).subscribe(res => {
        if (res.ok) {
          this.router.navigate(['cart/confirm']);
          this.cartContentNotificationService.update();
        }
      });
    }
  }

  getNbArticles(): number {
    let res = 0;
    this.cart!.orderLines!.forEach(element => (res += element.quantity || 0));
    return res;
  }
}

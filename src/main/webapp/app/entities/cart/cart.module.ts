import { NgModule } from '@angular/core';
import { CartComponent } from './cart.component';
import { SpaghettomatosSharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { MatTableModule } from '@angular/material/table';
import { CART_ROUTES } from './cart.route';
import { CartConfirmationComponent } from './cart-confirmation.component';
import { MatSortModule } from '@angular/material/sort';
import { CartPaymentComponent } from './cart-payment.component';

@NgModule({
  declarations: [CartComponent, CartConfirmationComponent, CartPaymentComponent],
  imports: [SpaghettomatosSharedModule, RouterModule.forChild(CART_ROUTES), MatTableModule, MatSortModule],
})
export class SpaghettomatosCartModule {}

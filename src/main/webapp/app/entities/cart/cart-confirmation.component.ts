import { Component, OnInit } from '@angular/core';
import { CartService } from './cart.service';
import { Router } from '@angular/router';
import { IProductOrder } from '../../shared/model/product-order.model';
import { IOrderLine } from '../../shared/model/order-line.model';

@Component({
  selector: 'jhi-cart-confirmation',
  templateUrl: './cart-confirmation.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartConfirmationComponent implements OnInit {
  cart?: IProductOrder | null = null;

  constructor(protected cartService: CartService, protected router: Router) {}

  ngOnInit(): void {}

  trackId(index: number, item: IOrderLine): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }
}

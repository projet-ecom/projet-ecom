import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { CartService } from './cart.service';
import { MatTableDataSource } from '@angular/material/table';
import { IOrderLine } from '../../shared/model/order-line.model';
import { Router } from '@angular/router';
import { FormControl, Validators, FormBuilder, FormArray, FormGroup } from '@angular/forms';
import { IProductOrder } from '../../shared/model/product-order.model';
import { CartContentNotificationService } from '../../shared/cartContentNotification/cartContentNotificationService';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'jhi-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit {
  cart?: IProductOrder;
  quantityControl = new FormControl(1, Validators.min(1));
  userForm!: FormGroup;
  displayedColumns = ['image', 'designation', 'unitPrice', 'quantity', 'totalPrice', 'delete'];
  dataSource: MatTableDataSource<IOrderLine> = new MatTableDataSource<IOrderLine>([]);
  requestFinished = false;

  constructor(
    protected cartService: CartService,
    protected router: Router,
    private fb: FormBuilder,
    private cartContentNotificationService: CartContentNotificationService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.cartService.getCart().subscribe((res: HttpResponse<IProductOrder>) => {
      this.dataSource.data = res.body?.orderLines || [];
      this.cart = res.body || undefined;
      this.userForm = this.fb.group([]);

      for (const orderLine of this.dataSource.data) {
        this.userForm.addControl(orderLine.id!.toString(), new FormControl(orderLine.quantity!, Validators.min(1)));
      }
      this.requestFinished = true;
    });
  }

  trackId(index: number, item: IOrderLine): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  openSuccessSnackBar(message: string): void {
    this.snackBar.open(message, 'x', { duration: 5000, panelClass: ['spaghettsuccess', 'snack-bar'], verticalPosition: 'top' });
  }

  openErrorSnackBar(message: string): void {
    this.snackBar.open(message, 'x', { duration: 5000, panelClass: ['spaghetterror', 'snack-bar'], verticalPosition: 'top' });
  }

  removeFromCart(orderLine: IOrderLine): void {
    if (orderLine.id) {
      this.cartService.removeFromCart(orderLine.id).subscribe(() => {
        this.ngOnInit();
        this.cartContentNotificationService.update();
        this.openSuccessSnackBar(orderLine?.product?.name + ' a été suprimé du panier');
        this.cartService.getCart().subscribe((res: HttpResponse<IProductOrder>) => {
          this.dataSource.data = res.body?.orderLines || [];
          this.cart = res.body || undefined;
        });
      });
    }
  }

  multiplyBy(num1: number, num2: number): number {
    return num1 * num2;
  }

  // push new form control when user clicks add button
  addNumber(): void {
    const control = this.userForm.controls['numbers'] as FormArray;
    control.push(new FormControl());
  }

  getFormQuantity(index: string): FormControl {
    return this.userForm.get(index) as FormControl;
  }

  totalSum(po: IProductOrder): number {
    let result = 0;

    for (const ol of po.orderLines!) {
      result += ol.product!.price! * ol.quantity!;
    }

    return result;
  }

  changeQuantity(element: IOrderLine): void {
    element.quantity = this.getFormQuantity(element.id!.toString()).value;
    this.cartService.modifyFromCart(element).subscribe(
      () => {
        this.ngOnInit();
        this.cartContentNotificationService.update();
        this.cartService.getCart().subscribe((res: HttpResponse<IProductOrder>) => {
          this.dataSource.data = res.body?.orderLines || [];
          this.cart = res.body || undefined;
        });
      },
      () => {
        this.openErrorSnackBar("Le produit n'est pas disponible en cette quantité");
        this.ngOnInit();
      }
    );
  }

  getNbArticles(): number {
    let res = 0;
    this.cart!.orderLines!.forEach(element => (res += element.quantity || 0));
    return res;
  }
}

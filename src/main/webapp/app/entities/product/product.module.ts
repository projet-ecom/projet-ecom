import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SpaghettomatosSharedModule } from 'app/shared/shared.module';
import { ProductComponent } from './product.component';
import { ProductDetailComponent } from './product-detail.component';
import { ProductUpdateComponent } from './product-update.component';
import { ProductDeleteDialogComponent } from './product-delete-dialog.component';
import { productRoute } from './product.route';
import { DisplayCategoryComponent } from './category-display.component';
import { MatGridListModule } from '@angular/material/grid-list';

@NgModule({
  imports: [SpaghettomatosSharedModule, RouterModule.forChild(productRoute), MatGridListModule],
  declarations: [ProductComponent, ProductDetailComponent, ProductUpdateComponent, ProductDeleteDialogComponent, DisplayCategoryComponent],
  entryComponents: [ProductDeleteDialogComponent],
})
export class SpaghettomatosProductModule {}

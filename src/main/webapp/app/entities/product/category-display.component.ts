import { Component, Input, OnInit } from '@angular/core';
import { ICategory } from '../../shared/model/category.model';
@Component({
  selector: 'jhi-category-diplay',
  template: `
    <div *ngIf="category">
      <div *ngIf="category.children">
        <div *ngFor="let child of category.children">
          <div id="display-child" *ngIf="child">
            {{ child.name }}
            <jhi-category-diplay [category]="child"></jhi-category-diplay>
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./category-display.component.scss'],
})
export class DisplayCategoryComponent implements OnInit {
  @Input() category: ICategory | null = null;

  ngOnInit(): void {}
}

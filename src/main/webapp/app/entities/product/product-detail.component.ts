import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { IProduct } from 'app/shared/model/product.model';
import { IOrderLine, OrderLine } from '../../shared/model/order-line.model';
import { CartService } from '../cart/cart.service';
import { ICategory } from '../../shared/model/category.model';
import { ProductService } from './product.service';
import { HttpResponse } from '@angular/common/http';
import { FormControl, Validators } from '@angular/forms';
import { AccountService } from '../../core/auth/account.service';
import { SignInNotificationService } from '../../shared/signInNotification/signInNotificationService';
import { CartContentNotificationService } from '../../shared/cartContentNotification/cartContentNotificationService';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'jhi-product-detail',
  templateUrl: './product-detail.component.html',
})
export class ProductDetailComponent implements OnInit {
  product: IProduct | null = null;
  orderLine: OrderLine | undefined;
  quantityControl = new FormControl(1, Validators.min(1));
  category: ICategory | null = null;

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected cartService: CartService,
    private router: Router,
    protected productService: ProductService,
    public accountService: AccountService,
    private signInNotificationService: SignInNotificationService,
    private cartContentNotificationService: CartContentNotificationService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ product, category }) => {
      this.product = product;
      this.category = category;

      if (this.accountService.hasAnyAuthority(['ROLE_USER'])) {
        this.cartService
          .getOrderLine(this.product!.id!)
          .subscribe((res: HttpResponse<IOrderLine>) => (this.orderLine = res.body || undefined));
      }
    });
  }

  loadProduct(): void {
    this.productService.find(this.product?.id || -1).subscribe(value => {
      this.product = value.body;
    });
  }

  openSignInMenu(): void {
    this.signInNotificationService.open();
  }

  previousState(): void {
    window.history.back();
  }

  openSuccessSnackBar(message: string): void {
    this.snackBar.open(message, 'x', { duration: 5000, panelClass: ['spaghettsuccess', 'snack-bar'], verticalPosition: 'top' });
  }

  openErrorSnackBar(message: string): void {
    this.snackBar.open(message, 'x', { duration: 5000, panelClass: ['spaghetterror', 'snack-bar'], verticalPosition: 'top' });
  }

  addToCart(product: IProduct): void {
    if (this.accountService.hasAnyAuthority('ROLE_USER')) {
      if (!this.orderLine) {
        this.orderLine = new OrderLine(undefined, this.quantityControl.value, product, undefined);
        this.cartService.addToCart(this.orderLine).subscribe(
          res => {
            this.orderLine = res.body || undefined;
            this.cartContentNotificationService.update();
            this.openSuccessSnackBar(this.orderLine?.product?.name + ' a bien été ajouté à votre panier.');
            this.loadProduct();
          },
          error => {
            this.openErrorSnackBar("Le produit n'est pas disponible en cette quantité");
            this.orderLine = undefined;
          }
        );
      } else {
        this.orderLine.quantity! += this.quantityControl.value;
        this.cartService.modifyFromCart(this.orderLine).subscribe(
          () => {
            this.cartContentNotificationService.update();
            this.openSuccessSnackBar(this.orderLine?.product?.name + ' a bien été ajouté à votre panier.');
            this.loadProduct();
          },
          error => {
            this.openErrorSnackBar("Le produit n'est pas disponible en cette quantité");
          }
        );
      }
    } else if (!this.accountService.hasAnyAuthority('ROLE_ADMIN')) {
      this.openSignInMenu();
      this.openErrorSnackBar('Vous devez être connecté pour commander un article.');
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { OrderHistoryService } from 'app/entities/order-history/order-history.service';
import { HttpResponse } from '@angular/common/http';
import { IProductOrder } from 'app/shared/model/product-order.model';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'jhi-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.scss'],
})

/**
 * @author Louka Soret
 */
export class OrderHistoryComponent implements OnInit {
  displayedColumns: string[] = ['id', 'creationDate', 'status', 'totalPrice', 'view'];
  dataSource: MatTableDataSource<IProductOrder> = new MatTableDataSource<IProductOrder>([]);
  requestFinished = false;

  constructor(protected orderHistoryService: OrderHistoryService) {}

  ngOnInit(): void {
    this.orderHistoryService
      .query()
      .subscribe((res: HttpResponse<IProductOrder[]>) => ((this.dataSource.data = res.body || []), (this.requestFinished = true)));
  }

  trackId(index: number, item: IProductOrder): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  totalSum(po: IProductOrder): number {
    let result = 0;

    for (const ol of po.orderLines!) {
      result += ol.product!.price! * ol.quantity!;
    }

    return result;
  }
}

import { NgModule } from '@angular/core';
import { OrderHistoryComponent } from './order-history.component';
import { SpaghettomatosSharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { OrderHistoryRoute } from './order-history.route';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';

@NgModule({
  declarations: [OrderHistoryComponent],
  imports: [SpaghettomatosSharedModule, RouterModule.forChild([OrderHistoryRoute]), MatTableModule, MatSortModule],
})

/**
 * @author Louka Soret
 */
export class SpaghettomatosOrderHistoryModule {}

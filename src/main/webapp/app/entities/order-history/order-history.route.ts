import { Route } from '@angular/router';

import { UserRouteAccessService } from '../../core/auth/user-route-access-service';
import { Authority } from '../../shared/constants/authority.constants';
import { OrderHistoryComponent } from './order-history.component';

/**
 * @author Louka Soret
 */
export const OrderHistoryRoute: Route = {
  path: '',
  component: OrderHistoryComponent,
  data: {
    authorities: [Authority.USER],
    pageTitle: 'order-history.title',
  },
  canActivate: [UserRouteAccessService],
};

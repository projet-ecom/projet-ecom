import { Injectable } from '@angular/core';

import { SERVER_API_URL } from 'app/app.constants';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IProductOrder } from 'app/shared/model/product-order.model';
import { ICustomer } from 'app/shared/model/customer.model';

type EntityResponseType = HttpResponse<ICustomer>;
type EntityArrayResponseType = HttpResponse<ICustomer[]>;

/**
 * @author Louka Soret
 */
@Injectable({ providedIn: 'root' })
export class OrderHistoryService {
  public resourceUrl = SERVER_API_URL + 'api/order-history';

  constructor(protected http: HttpClient) {}

  query(): Observable<EntityArrayResponseType> {
    return this.http.get<IProductOrder[]>(this.resourceUrl, { observe: 'response' });
  }
}

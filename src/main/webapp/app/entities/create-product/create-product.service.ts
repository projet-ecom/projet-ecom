import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IProduct } from 'app/shared/model/product.model';
import { ICategory } from '../../shared/model/category.model';

type EntityResponseType = HttpResponse<IProduct>;
type EntityArrayResponseType = HttpResponse<IProduct[]>;

@Injectable({ providedIn: 'root' })
export class CreateProductService {
  public resourceUrl = SERVER_API_URL + 'api/products';
  public resourceUrlCategory = SERVER_API_URL + 'api/categories';

  constructor(protected http: HttpClient) {}

  create(product: IProduct): Observable<EntityResponseType> {
    return this.http.post<IProduct>(this.resourceUrl, product, { observe: 'response' });
  }

  update(product: IProduct): Observable<EntityResponseType> {
    return this.http.put<IProduct>(this.resourceUrl, product, { observe: 'response' });
  }

  updateProductWithCategory(product: IProduct): Observable<EntityResponseType> {
    return this.http.put<IProduct>(this.resourceUrl + '/updateProductWithCategory', product, { observe: 'response' });
  }

  findProduct(id: number): Observable<EntityResponseType> {
    return this.http.get<IProduct>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  findCategory(id: number): Observable<ICategory> {
    return this.http.get<ICategory>(`${this.resourceUrlCategory}/${id}`);
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IProduct[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  findMaterial(): Observable<ICategory> {
    return this.http.get<ICategory>(`${this.resourceUrlCategory}/findMaterial`);
  }

  findChildren(idCategory: number): Observable<ICategory[]> {
    return this.http.get<ICategory[]>(`${this.resourceUrlCategory}/findChildren/${idCategory}`);
  }

  createProductWithCategory(product: IProduct): Observable<EntityResponseType> {
    return this.http.post<IProduct>(this.resourceUrl + '/createProductWithCategory', product, { observe: 'response' });
  }
}

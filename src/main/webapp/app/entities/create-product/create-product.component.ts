import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IProduct, Product } from 'app/shared/model/product.model';
import { CreateProductService } from './create-product.service';
import { ICategory } from '../../shared/model/category.model';

@Component({
  selector: 'jhi-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.scss'],
})
export class CreateProductComponent implements OnInit {
  isSaving = false;
  materials: ICategory | null = null;
  categoriesTree: ICategory | null = null;
  categoryDisplay: ICategory[] = [];
  numIndexCategory: number;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    stockQuantity: [null, [Validators.required, Validators.min(0)]],
    price: [null, [Validators.required, Validators.min(0)]],
    description: [],
    imagePath: [],
    ICategory: [],
  });

  constructor(protected productService: CreateProductService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {
    this.numIndexCategory = 0;
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ product, materials, categoriesTree }) => {
      this.updateForm(product);
      this.materials = materials;
      this.categoriesTree = categoriesTree;
    });
  }

  updateForm(product: IProduct): void {
    this.editForm.patchValue({
      id: product.id,
      name: product.name,
      stockQuantity: product.stockQuantity,
      price: product.price,
      description: product.description,
      imagePath: product.imagePath,
      categories: product.categories,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const product = this.createFromForm();
    if (product.id !== undefined) {
      this.subscribeToSaveResponse(this.productService.updateProductWithCategory(product));
    } else {
      this.subscribeToSaveResponse(this.productService.createProductWithCategory(product));
    }
  }

  private createFromForm(): IProduct {
    return {
      ...new Product(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      stockQuantity: this.editForm.get(['stockQuantity'])!.value,
      price: this.editForm.get(['price'])!.value,
      description: this.editForm.get(['description'])!.value,
      imagePath: this.editForm.get(['imagePath'])!.value,
      categories: this.categoryDisplay,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProduct>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  processReq(categoryDown: ICategory): void {
    this.numIndexCategory = 0;
    this.categoryDisplay = [];
    this.fillCategory(categoryDown);
  }

  fillCategory(categoryDown: ICategory): void {
    if (categoryDown !== undefined && categoryDown.id !== -1) {
      this.categoryDisplay[this.numIndexCategory] = categoryDown;
      if (categoryDown.children != null) {
        for (let i = 0; i < categoryDown.children.length; i++) {
          if (categoryDown.children[i] !== null && categoryDown.children[i] !== undefined) {
            this.numIndexCategory++;
            this.fillCategory(categoryDown.children[i]);
          }
        }
      }
    }
  }
}

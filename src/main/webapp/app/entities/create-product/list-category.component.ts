import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Category, ICategory } from '../../shared/model/category.model';
import { CreateProductService } from './create-product.service';
@Component({
  selector: 'jhi-list-category',
  template: `
    <div *ngIf="!pushBtnDelete">
      <div class="example-container" *ngIf="categoryParent">
        <span *ngIf="!isMaterial">L</span>
        <mat-form-field id="choose-child" appearance="fill" floatPlaceholder="never">
          <mat-label>Sélectionner votre caregorie</mat-label>
          <mat-select
            matNativeControl
            id="mySelectId"
            (selectionChange)="chooseCategory(selectedCategory.value, true)"
            value="{{ selectedValueForUpdate }}"
            #selectedCategory
          >
            <mat-option value="" disabled selected></mat-option>
            <mat-option *ngFor="let child of categoryParent.children" value="{{ child.id }}">
              {{ child.name }}
            </mat-option>
          </mat-select>
        </mat-form-field>
        <button mat-raised-button color="primary" (click)="addCategoryChildUser()" [disabled]="!hasChild || !chooseCat">
          Add
        </button>
        <button mat-raised-button color="primary" (click)="deleteCategory()" *ngIf="!isMaterial">Delete</button>
      </div>
      <div class="child-category" *ngFor="let item of [].constructor(nbChildUpdate); let i = index">
        <div *ngIf="categoriesTreeUpdate">
          <div *ngIf="categoriesTreeUpdate.children">
            <jhi-list-category
              (sendRequestToData)="processReq($event, i)"
              [categoryParent]="categorySelect"
              [isUpdate]="isUpdate"
              [categoriesTreeUpdate]="categoriesTreeUpdate.children[i]"
            ></jhi-list-category>
          </div>
        </div>
      </div>
      <div class="child-category" *ngFor="let item of [].constructor(nbChildUser); let i = index">
        <jhi-list-category (sendRequestToData)="processReq($event, i + 100)" [categoryParent]="categorySelect"></jhi-list-category>
      </div>
    </div>
  `,
  styleUrls: ['list-category.component.scss'],
})
export class CategoryListComponent implements OnInit {
  @Input() categoryParent: ICategory | null = null;
  @Input() isMaterial: boolean | null = false;
  @Input() isUpdate: boolean | null = false;
  @Input() categoriesTreeUpdate: ICategory | null = null;
  @Output() sendRequestToData = new EventEmitter();

  pushBtnDelete: boolean | undefined;
  chooseCat: boolean | undefined;
  nbChildUser: number;
  nbChildUpdate: number;
  valSelect: number;
  categorySelect: ICategory | null = null;
  hasChild: boolean;

  categoryInfo: ICategory | null = null;

  categoryForUpdate: ICategory | null = null;
  selectedValueForUpdate: number | undefined;

  lockUpdate: boolean | null = false;

  constructor(private service: CreateProductService) {
    this.pushBtnDelete = false;
    this.chooseCat = false;
    this.nbChildUser = 0;
    this.nbChildUpdate = 0;
    this.valSelect = -1;
    this.hasChild = true;
    this.selectedValueForUpdate = 0;
    this.lockUpdate = false;
  }

  ngOnInit(): void {
    if (this.isUpdate) {
      this.actionUpdate();
    }
  }

  addCategoryChildUser(): void {
    this.nbChildUser++;
  }

  addCategoryChildUpdate(): void {
    this.nbChildUpdate++;
  }

  deleteCategory(): void {
    this.pushBtnDelete = true;
    this.sendRequestToData.emit(new Category(-1, '', '', [], [], new Category()));
  }

  chooseCategory(val: string, isUser: boolean): void {
    this.nbChildUpdate = 0;
    this.nbChildUser = 0;
    this.chooseCat = true;
    this.valSelect = +val;
    this.service.findCategory(this.valSelect).subscribe(categorySelect => {
      this.categorySelect = categorySelect;
      if (categorySelect == null || categorySelect.children == null || categorySelect.children.length === 0) {
        this.hasChild = false;
      } else {
        this.hasChild = true;
      }

      if (this.categoryParent != null) {
        this.categoryInfo = new Category(this.categorySelect.id, this.categorySelect.name, '', [], [], new Category());
        this.sendRequestToData.emit(this.categoryInfo);
      }

      if (!isUser) {
        if (this.categoriesTreeUpdate != null && this.categoriesTreeUpdate.children) {
          for (let i = 0; i < this.categoriesTreeUpdate.children.length; i++) {
            this.addCategoryChildUpdate();
          }
        }
      }
    });
  }

  processReq(categoryDown: ICategory, numChild: number): void {
    if (this.categoryParent != null) {
      if (this.categoryInfo == null) {
        this.categoryInfo = new Category(this.categoryParent.id, this.categoryParent.name, '', [], [], new Category());
      }
      if (this.categoryInfo.children != null) {
        this.categoryInfo.children[+numChild] = categoryDown;
      }

      this.sendRequestToData.emit(this.categoryInfo);
    }
  }

  actionUpdate(): void {
    if (this.categoryParent != null && this.categoriesTreeUpdate != null && this.categoriesTreeUpdate.id != null) {
      this.selectedValueForUpdate = this.categoriesTreeUpdate.id;
      this.chooseCategory(this.categoriesTreeUpdate.id.toString(), false);
    }
  }
}

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SpaghettomatosSharedModule } from 'app/shared/shared.module';
import { CreateProductComponent } from './create-product.component';
import { createProductRoute } from './create-product.route';
import { CategoryListComponent } from './list-category.component';

@NgModule({
  imports: [SpaghettomatosSharedModule, RouterModule.forChild(createProductRoute)],
  declarations: [CreateProductComponent, CategoryListComponent],
  entryComponents: [],
})
export class SpaghettomatosCreateProductModule {}

import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IProduct, Product } from 'app/shared/model/product.model';
import { CreateProductService } from './create-product.service';
import { CreateProductComponent } from './create-product.component';
import { Category, ICategory } from '../../shared/model/category.model';
import { CategoryService } from '../category/category.service';

@Injectable({ providedIn: 'root' })
export class ProductResolve implements Resolve<IProduct> {
  constructor(private service: CreateProductService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IProduct> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.findProduct(id).pipe(
        flatMap((product: HttpResponse<Product>) => {
          if (product.body) {
            return of(product.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Product());
  }
}

@Injectable({ providedIn: 'root' })
export class CategoryMaterialResolve implements Resolve<ICategory> {
  constructor(private service: CreateProductService, private router: Router) {}

  resolve(): Observable<ICategory> {
    return this.service.findMaterial();
  }
}

@Injectable({ providedIn: 'root' })
export class CategoryResolve implements Resolve<ICategory> {
  constructor(private service: CategoryService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICategory> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.findCategoriesByProduct(id);
    }
    return of(new Category());
  }
}

export const createProductRoute: Routes = [
  {
    path: '',
    component: CreateProductComponent,
    resolve: {
      product: ProductResolve,
      materials: CategoryMaterialResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      defaultSort: 'id,asc',
      pageTitle: 'spaghettomatosApp.create-product.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CreateProductComponent,
    resolve: {
      product: ProductResolve,
      materials: CategoryMaterialResolve,
      categoriesTree: CategoryResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      defaultSort: 'id,asc',
      pageTitle: 'spaghettomatosApp.create-product-view.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SpaghettomatosSharedModule } from 'app/shared/shared.module';
import { ProductOrderComponent } from './product-order.component';
import { ProductOrderDetailComponent } from './product-order-detail.component';
import { ProductOrderUpdateComponent } from './product-order-update.component';
import { ProductOrderDeleteDialogComponent } from './product-order-delete-dialog.component';
import { productOrderRoute } from './product-order.route';
import { MatTableModule } from '@angular/material/table';

@NgModule({
  imports: [SpaghettomatosSharedModule, RouterModule.forChild(productOrderRoute), MatTableModule],
  declarations: [ProductOrderComponent, ProductOrderDetailComponent, ProductOrderUpdateComponent, ProductOrderDeleteDialogComponent],
  entryComponents: [ProductOrderDeleteDialogComponent],
  exports: [ProductOrderDetailComponent],
})
export class SpaghettomatosProductOrderModule {}

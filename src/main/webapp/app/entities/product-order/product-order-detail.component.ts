import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IProductOrder } from 'app/shared/model/product-order.model';
import { AccountService } from '../../core/auth/account.service';
import { IOrderLine } from '../../shared/model/order-line.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MatTableDataSource } from '@angular/material/table';
import { OrderLineService } from '../order-line/order-line.service';
import { ProductOrderService } from './product-order.service';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'jhi-product-order-detail',
  templateUrl: './product-order-detail.component.html',
  styleUrls: ['./product-order-detail.scss'],
})
export class ProductOrderDetailComponent implements OnInit {
  productOrder: IProductOrder | null = null;
  displayedColumns = ['image', 'designation', 'unitPrice', 'quantity', 'totalPrice'];
  dataSource: MatTableDataSource<IOrderLine> = new MatTableDataSource<IOrderLine>([]);
  requestFinished = false;

  constructor(
    protected orderLineService: OrderLineService,
    protected productOrderService: ProductOrderService,
    private accountService: AccountService,
    protected activatedRoute: ActivatedRoute,
    protected modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ productOrder }) => {
      this.productOrder = productOrder;
      this.dataSource.data = productOrder?.orderLines || [];
      this.requestFinished = true;
      if (this.accountService.hasAnyAuthority(['ROLE_ADMIN'])) {
        this.displayedColumns.push('delete');
      }
    });
  }

  trackId(index: number, item: IOrderLine): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  delete(orderLine: IOrderLine): void {
    this.requestFinished = true;
    this.orderLineService.delete(orderLine.id!).subscribe(() => {
      this.dataSource = new MatTableDataSource<IOrderLine>([]);
      this.productOrderService.find(this.productOrder!.id!).subscribe((res: HttpResponse<IProductOrder>) => {
        this.productOrder = res.body;
        this.dataSource.data = res.body?.orderLines || [];
        this.requestFinished = true;
      });
    });
  }

  previousState(): void {
    window.history.back();
  }

  multiplyBy(num1: number, num2: number): number {
    return num1 * num2;
  }

  totalSum(po: IProductOrder): number {
    let result = 0;

    for (const ol of po.orderLines!) {
      result += ol.product!.price! * ol.quantity!;
    }

    return result;
  }
}

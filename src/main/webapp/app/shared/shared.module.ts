import { NgModule } from '@angular/core';
import { SpaghettomatosSharedLibsModule } from './shared-libs.module';
import { FindLanguageFromKeyPipe } from './language/find-language-from-key.pipe';
import { AlertComponent } from './alert/alert.component';
import { AlertErrorComponent } from './alert/alert-error.component';
import { HasAnyAuthorityDirective } from './auth/has-any-authority.directive';
import { JhMaterialModule } from 'app/shared/jh-material.module';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [SpaghettomatosSharedLibsModule, JhMaterialModule, CommonModule],
  declarations: [FindLanguageFromKeyPipe, AlertComponent, AlertErrorComponent, HasAnyAuthorityDirective],
  exports: [
    SpaghettomatosSharedLibsModule,
    FindLanguageFromKeyPipe,
    AlertComponent,
    AlertErrorComponent,
    HasAnyAuthorityDirective,
    JhMaterialModule,
    CommonModule,
  ],
})
export class SpaghettomatosSharedModule {}

import { EventEmitter, Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class CartContentNotificationService {
  public cartValueEmitter: EventEmitter<void>;
  constructor() {
    this.cartValueEmitter = new EventEmitter<void>();
  }
  update(): void {
    this.cartValueEmitter.emit();
  }
}

import { IProduct } from 'app/shared/model/product.model';

export interface ICategory {
  id?: number;
  name?: string;
  description?: string;
  children?: ICategory[];
  products?: IProduct[];
  parent?: ICategory;
}

export class Category implements ICategory {
  constructor(
    public id?: number,
    public name?: string,
    public description?: string,
    public children?: ICategory[],
    public products?: IProduct[],
    public parent?: ICategory
  ) {}
}

import { IProduct } from 'app/shared/model/product.model';
import { IProductOrder } from 'app/shared/model/product-order.model';

export interface IOrderLine {
  id?: number;
  quantity?: number;
  product?: IProduct;
  productOrder?: IProductOrder;
}

export class OrderLine implements IOrderLine {
  constructor(public id?: number, public quantity?: number, public product?: IProduct, public productOrder?: IProductOrder) {}
}

import { Moment } from 'moment';
import { IUser } from 'app/core/user/user.model';
import { IProductOrder } from 'app/shared/model/product-order.model';

export interface ICustomer {
  id?: number;
  birthDate?: Moment;
  user?: IUser;
  orders?: IProductOrder[];
}

export class Customer implements ICustomer {
  constructor(public id?: number, public birthDate?: Moment, public user?: IUser, public orders?: IProductOrder[]) {}
}

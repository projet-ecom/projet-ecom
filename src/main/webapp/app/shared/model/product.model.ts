import { ICategory } from 'app/shared/model/category.model';

export interface IProduct {
  id?: number;
  name?: string;
  stockQuantity?: number;
  price?: number;
  description?: string;
  imagePath?: string;
  categories?: ICategory[];
}

export class Product implements IProduct {
  constructor(
    public id?: number,
    public name?: string,
    public stockQuantity?: number,
    public price?: number,
    public description?: string,
    public imagePath?: string,
    public categories?: ICategory[]
  ) {}
}

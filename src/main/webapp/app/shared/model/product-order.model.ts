import { Moment } from 'moment';
import { IOrderLine } from 'app/shared/model/order-line.model';
import { ICustomer } from 'app/shared/model/customer.model';
import { OrderStatus } from 'app/shared/model/enumerations/order-status.model';

export interface IProductOrder {
  id?: number;
  creationDate?: Moment;
  status?: OrderStatus;
  orderLines?: IOrderLine[];
  customer?: ICustomer;
}

export class ProductOrder implements IProductOrder {
  constructor(
    public id?: number,
    public creationDate?: Moment,
    public status?: OrderStatus,
    public orderLines?: IOrderLine[],
    public customer?: ICustomer
  ) {}
}

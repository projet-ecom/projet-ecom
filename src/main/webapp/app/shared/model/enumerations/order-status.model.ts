export const enum OrderStatus {
  CART = 'CART',

  COMPLETED = 'COMPLETED',

  CANCELLED = 'CANCELLED',
}

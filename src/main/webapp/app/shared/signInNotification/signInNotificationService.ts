import { EventEmitter, Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class SignInNotificationService {
  public menuEmitter: EventEmitter<boolean>;
  constructor() {
    this.menuEmitter = new EventEmitter<boolean>();
  }
  open(): void {
    this.menuEmitter.emit(true);
  }
  close(): void {
    this.menuEmitter.emit(false);
  }
}

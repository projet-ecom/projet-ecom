import { Component, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { JhiLanguageService } from 'ng-jhipster';

import { EMAIL_ALREADY_USED_TYPE } from 'app/shared/constants/error.constants';
import { RegisterService } from './register.service';

import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

@Component({
  selector: 'jhi-register',
  templateUrl: './register.component.html',
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ],
})
export class RegisterComponent implements AfterViewInit {
  @ViewChild('login', { static: false })
  login?: ElementRef;
  hide = true;
  hide2 = true;
  passwordmaxlenght = 25;

  maxDate: Date;

  doNotMatch = false;
  error = false;
  errorEmailExists = false;
  errorUserExists = false;
  success = false;

  registerForm = this.fb.group({
    firstName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(this.passwordmaxlenght)]],
    lastName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(this.passwordmaxlenght)]],
    email: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(254), Validators.email]],
    password: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(this.passwordmaxlenght)]],
    confirmPassword: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(this.passwordmaxlenght)]],
    birthdayDate: ['', [Validators.required]],
  });

  constructor(private languageService: JhiLanguageService, private registerService: RegisterService, private fb: FormBuilder) {
    const currentYear = new Date().getFullYear();
    const currentMont = new Date().getMonth();
    const currentDay = new Date().getDay();
    this.maxDate = new Date(currentYear - 18, currentMont, currentDay);
  }

  ngAfterViewInit(): void {
    if (this.login) {
      this.login.nativeElement.focus();
    }
  }

  passwordLenght(): void {
    return this.registerForm.get(['password'])!.value.lenght;
  }

  register(): void {
    this.doNotMatch = false;
    this.error = false;
    this.errorEmailExists = false;
    this.errorUserExists = false;

    const password = this.registerForm.get(['password'])!.value;
    if (password !== this.registerForm.get(['confirmPassword'])!.value) {
      this.doNotMatch = true;
    } else {
      const login = this.registerForm.get(['email'])!.value;
      const email = this.registerForm.get(['email'])!.value;
      const lastName = this.registerForm.get(['lastName'])!.value;
      const firstName = this.registerForm.get(['firstName'])!.value;
      const birthday = this.registerForm.get(['birthdayDate'])!.value;
      this.registerService
        .save({ login, email, password, birthday, lastName, firstName, langKey: this.languageService.getCurrentLanguage() })
        .subscribe(
          () => (this.success = true),
          response => this.processError(response)
        );
    }
  }

  openLogin(): void {}

  private processError(response: HttpErrorResponse): void {
    if (response.status === 400 && response.error.type === EMAIL_ALREADY_USED_TYPE) {
      this.errorEmailExists = true;
    } else {
      this.error = true;
    }
  }
}

package com.ecomproject.spaghettomatos.service;

import com.ecomproject.spaghettomatos.domain.OrderLine;
import com.ecomproject.spaghettomatos.domain.User;
import com.ecomproject.spaghettomatos.service.errors.OrderLineException;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link OrderLine}.
 */
public interface OrderLineService {

    /**
     * Save a orderLine.
     *
     * @param orderLine the entity to save.
     * @return the persisted entity.
     */
    OrderLine save(OrderLine orderLine);

    /**
     * Get all the orderLines.
     *
     * @return the list of entities.
     */
    List<OrderLine> findAll();


    /**
     * Get the "id" orderLine.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<OrderLine> findOne(Long id);

    /**
     * Delete the "id" orderLine.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Find the user of the OrderLine
     *
     * @param id the id of the entity.
     * @return User the user of the OrderLine
     */
	Optional<User> findUser(Long id);

    /**
     * Find the orderline in the cart of the user fro a product
     *
     * @param productId the id of the product.
     * @param login the user login
     * @return OrderLine the orderline
     */
	Optional<OrderLine> findOrderLineInCartByProductAndUser(Long productId, String login);

    /**
     * add a product to the cart
     * @param orderLine the orderLine to create
     * @return OrderLine The orderline created or updated
     */
    Long addToCart(OrderLine orderLine) throws OrderLineException;

    /**
     * remove a product from the cart
     * @param id the id of the orderLine to remove
     */
    void removeFromCart(Long id) throws OrderLineException;

    /**
     * Find the OrderLines of the order orderId
     *
     * @author Louka Soret
     * @param orderId the order id
     * @return The list of orderlines in the order
     */
    List<OrderLine> findByOrderId(Long orderId);
}

package com.ecomproject.spaghettomatos.service.errors;

public class OrderLineException extends Exception{
    public OrderLineException(String message) {
        super(message);
    }
}

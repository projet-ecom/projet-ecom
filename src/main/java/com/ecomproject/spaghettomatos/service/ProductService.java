package com.ecomproject.spaghettomatos.service;

import com.ecomproject.spaghettomatos.domain.Category;
import com.ecomproject.spaghettomatos.domain.Product;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Service Interface for managing {@link Product}.
 */
public interface ProductService {

    /**
     * Save a product.
     *
     * @param product the entity to save.
     * @return the persisted entity.
     */
    Product save(Product product);

    /**
     * Get all the products.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Product> findAll(Pageable pageable);


    /**
     * Get the "id" product.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Product> findOne(Long id);

    /**
     * Get the "id" product.
     *
     * @param id the id of the entity.
     * @return the entity and categories.
     */
    Optional<Product> findOneWithCategories(Long id);

    /**
     * Delete the "id" product.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    Product createProductWithCategory(Product product, Set<Category> categories);

    Product updateProductWithCategory(Product product, Set<Category> categories);
}

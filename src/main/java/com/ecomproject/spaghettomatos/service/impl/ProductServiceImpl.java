package com.ecomproject.spaghettomatos.service.impl;

import com.ecomproject.spaghettomatos.domain.Category;
import com.ecomproject.spaghettomatos.domain.Product;
import com.ecomproject.spaghettomatos.repository.ProductRepository;
import com.ecomproject.spaghettomatos.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Service Implementation for managing {@link Product}.
 */
@Service
@Transactional
public class ProductServiceImpl implements ProductService {

    private final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);

    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Product save(Product product) {
        log.debug("Request to save Product : {}", product);
        return productRepository.save(product);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Product> findAll(Pageable pageable) {
        log.debug("Request to get all Products");
        return productRepository.findAll(pageable);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<Product> findOne(Long id) {
        log.debug("Request to get Product : {}", id);
        return productRepository.findById(id);
    }

    @Transactional(readOnly = true)
    public Optional<Product> findOneWithCategories(Long id) {
        log.debug("Request to get Product with categories : {}", id);
        return productRepository.findOneProductWithCategories(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Product : {}", id);
        Optional<Product> prod = productRepository.findById(id);
        Product product = prod.get();
        List<Category> categoriesOfProduct = productRepository.findAllCategoriesWithIdProduct(id);

        for(Category category: categoriesOfProduct){
            category.removeProduct(product);
        }
        productRepository.deleteById(id);
    }

    public Product createProductWithCategory(Product product, Set<Category> categories){

        Product prod = productRepository.save(product);

        for (Category cat : categories){
            log.debug("Cat: {}",cat.getName());
            Optional<Category> categoryBDD = productRepository.getOneCategory(cat.getId());
            categoryBDD.get().addProduct(prod);
        }

        return prod;
    }

    public Product updateProductWithCategory(Product product, Set<Category> categories){


        Product prod = productRepository.save(product);

        //delete all product from category
        List<Category> categoryWithProduct = productRepository.findAllCategoriesWithIdProduct(product.getId());
        for(Category cat : categoryWithProduct){
            cat.removeProduct(prod);
        }

        for (Category cat : categories){
            if(cat!=null) {
                log.debug("Cat: {}", cat.getName());
                Optional<Category> categoryBDD = productRepository.getOneCategory(cat.getId());
                categoryBDD.get().addProduct(prod);
            }
        }

        return prod;
    }
}

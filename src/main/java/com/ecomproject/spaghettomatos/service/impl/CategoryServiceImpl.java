package com.ecomproject.spaghettomatos.service.impl;

import com.ecomproject.spaghettomatos.domain.Category;
import com.ecomproject.spaghettomatos.domain.Product;
import com.ecomproject.spaghettomatos.repository.CategoryRepository;
import com.ecomproject.spaghettomatos.service.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Service Implementation for managing {@link Category}.
 */
@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

    private final Logger log = LoggerFactory.getLogger(CategoryServiceImpl.class);

    private final CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Category save(Category category) {
        log.debug("Request to save Category : {}", category);
        if(category.getParent()!=null){
            if(category.getParent().getChildren()!=null){
                boolean notChild = true;
                for (Category c : category.getParent().getChildren()) {
                    if (c.getId() == category.getId()) {
                        notChild = false;
                        break;
                    }
                }
                if (notChild){
                    Optional<Category> categoryParent = categoryRepository.findOneWithEagerRelationships(category.getParent().getId());
                    categoryParent.get().addChildren(category);
                }
            }else{
                Set<Category> childrenList = new HashSet<>();
                category.getParent().setChildren(childrenList);
                Optional<Category> categoryParent = categoryRepository.findOneWithEagerRelationships(category.getParent().getId());
                categoryParent.get().addChildren(category);
            }


        }
        return categoryRepository.save(category);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Category> findAll() {
        log.debug("Request to get all Categories");
        return categoryRepository.findAllWithEagerRelationships();
    }


    public Page<Category> findAllWithEagerRelationships(Pageable pageable) {
        return categoryRepository.findAllWithEagerRelationships(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Category> findOne(Long id) {
        log.debug("Request to get Category : {}", id);
        Optional<Category> cat = categoryRepository.findOneWithEagerRelationships(id);

        if(cat.equals(Optional.empty())){
            return cat;
        }
        //initialization origine
        Category category=cat.get();
        Category categoryReturn = new Category(category.getId(),category.getName(),category.getDescription(),category.getProducts(),category.getParent(),null);

        Set<Category> listChild = new HashSet<>();
        categoryReturn.setChildren(listChild);
        for (Category material : category.getChildren()) {
            listChild.add(new Category(material.getId(),material.getName(),material.getDescription(),null,null,null));
        }

        return Optional.of(categoryReturn);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Category : {}", id);
        Optional<Category> cat = categoryRepository.findOneWithEagerRelationships(id);
        Category category = cat.get();

        //product
        List<Long> idListProduct = new ArrayList<>();
        for (Product prod : category.getProducts()){
            idListProduct.add(prod.getId());
        }
        for (Long idProd : idListProduct){
            Optional<Product> p = categoryRepository.findOneProduct(idProd);
            Product product = p.get();
            product.removeCategory(category);
        }
        //parent
        if(category.getParent()!=null){
            Optional<Category> catParent = categoryRepository.findOneWithEagerRelationships(category.getParent().getId());
            Category categoryParent = catParent.get();
            categoryParent.removeChildren(category);
        }

        categoryRepository.deleteById(id);
    }

    public Category getMaterial(){
        log.debug("Request to getMaterial Category");

        List<Category> categoryMaterial = categoryRepository.getMaterial();


        if(categoryMaterial.equals(Optional.empty())){
            return new Category();
        }
        Category categoriesResult = new Category();

        Set<Category> listMaterial = new HashSet<>();
        categoriesResult.setChildren(listMaterial);
        for (Category material : categoryMaterial) {
            listMaterial.add(new Category(material.getId(),material.getName(),material.getDescription(),null,null,null));
        }
        return categoriesResult;
    }

    public Category[] getChildrenWithCategories(Long id){
        log.debug("Request to getChildrenWithCategories Category : {}", id);

        Optional<Category> categoryParent = categoryRepository.findOneWithEagerRelationships(id);


        if(categoryParent.equals(Optional.empty())){
            return new Category[0];
        }
        Category[] categoriesResult = new Category[categoryParent.get().getChildren().size()];
        int i=0;
        for (Category child : categoryParent.get().getChildren()) {
            categoriesResult[i] = new Category(child.getId(),child.getName(),child.getDescription(),null,null,null);
            i++;
        }

        return categoriesResult;
    }

    public Category findAllCategoriesWithProduct(Long idProduct) {
        log.debug("Request to get Category : {}", idProduct);

        Optional<Category> cat = categoryRepository.findTypeMaterial(idProduct);
        if(cat.equals(Optional.empty())){
            return new Category((long) 1,"Non affecté",null,null,null,null);
        }
        //initialization origine
        Category categoryResult=cat.get();
        Category categoryReturn = new Category(categoryResult.getId(),categoryResult.getName(),categoryResult.getDescription(),categoryResult.getProducts(),null,null);
        browseChildrenWithIdProduct(categoryResult,categoryReturn,idProduct);
        return categoryReturn;
    }

    void browseChildrenWithIdProduct(Category category, Category categoryCreate, Long idProduct){

        if (category.getChildren()!=null){
            try {
                for (Category cat1 : category.getChildren()) {
                    boolean okProduct = false;
                    for (Product product : cat1.getProducts()) {
                        if (product.getId().equals(idProduct)) {
                            okProduct = true;
                            break;
                        }
                    }
                    if (okProduct) {
                        Category cat_tmp = new Category(cat1.getId(), cat1.getName(), cat1.getDescription(), null, null, null);
                        if (categoryCreate.getChildren() == null) {
                            categoryCreate.setChildren(new HashSet<>());
                        }
                        categoryCreate.getChildren().add(cat_tmp);
                        browseChildrenWithIdProduct(cat1, cat_tmp, idProduct);
                    }
                }
            }catch (Exception e){

            }
        }
    }

    public Category getTreeCategory(){
        List<Category> categoryMaterial = categoryRepository.getMaterial();


        if(categoryMaterial.equals(Optional.empty())){
            return new Category();
        }

        Category categoriesRoot = new Category();

        Set<Category> listMaterial = new HashSet<>();
        categoriesRoot.setChildren(listMaterial);
        for (Category material : categoryMaterial) {
            listMaterial.add(new Category(material.getId(),material.getName(),material.getDescription(),null,null,material.getChildren()));
        }


        Category categoryReturn = new Category();

        browseChildren(categoriesRoot, categoryReturn);


        return categoryReturn;
    }

    void browseChildren(Category category, Category categoryCreate){
        if (category!=null && category.getChildren()!=null){
            for (Category cat1 : category.getChildren()){
                Category cat_tmp = new Category(cat1.getId(),cat1.getName(),cat1.getDescription(),null,null,null);
                if(categoryCreate.getChildren()==null){
                    categoryCreate.setChildren(new HashSet<>());
                }
                categoryCreate.getChildren().add(cat_tmp);
                browseChildren(cat1, cat_tmp);
            }
        }
    }

    public Page<Product> findAllCategoriesFilter(Long[] list_id, Pageable pageable){
        List<Product> productsSelect = new ArrayList<>();
        List<Product> productsResult = new ArrayList<>();
        if(list_id.length!=0) {
            productsSelect = categoryRepository.findAllCategoriesFilter(list_id[0]);
            for (int i=1;i<list_id.length;i++) {//parcourir tous les filtres
                List<Product> productsOfCategory = categoryRepository.findAllCategoriesFilter(list_id[i]);
                for (Product prod_tmp : productsOfCategory) {//parcourir tous les produits de ce filtre
                    boolean isInsideResult=false;
                    for (Product productSelect : productsSelect){//vérifier si le résultat se trouve dans la liste des résultats
                        if(productSelect.getId().equals(prod_tmp.getId())){
                            isInsideResult=true;
                            break;
                        }
                    }
                    if (isInsideResult) {
                        productsResult.add(prod_tmp);
                    }
                }
                productsSelect=productsResult;
                productsResult = new ArrayList<>();

            }
        }
        productsResult=productsSelect;
        List<Product> productsInPage = new ArrayList<>();
        int numPage=pageable.getPageNumber();
        int nbProduct=pageable.getPageSize();
        for (int i=numPage*nbProduct; i<(numPage+1)*nbProduct && i<productsResult.size(); i++){
            productsInPage.add(productsResult.get(i));
        }

        Page page = new PageImpl(productsInPage,pageable,productsResult.size());
        return page;
    }

}

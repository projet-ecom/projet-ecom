package com.ecomproject.spaghettomatos.service.impl;

import com.ecomproject.spaghettomatos.domain.Product;
import com.ecomproject.spaghettomatos.domain.enumeration.OrderStatus;
import com.ecomproject.spaghettomatos.repository.ProductRepository;
import com.ecomproject.spaghettomatos.service.OrderLineService;
import com.ecomproject.spaghettomatos.domain.OrderLine;
import com.ecomproject.spaghettomatos.domain.User;
import com.ecomproject.spaghettomatos.repository.OrderLineRepository;
import com.ecomproject.spaghettomatos.service.errors.OrderLineException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link OrderLine}.
 */
@Service
@Transactional
public class OrderLineServiceImpl implements OrderLineService {

    private final Logger log = LoggerFactory.getLogger(OrderLineServiceImpl.class);

    private final OrderLineRepository orderLineRepository;
    private ProductRepository productRepository;

    public OrderLineServiceImpl(OrderLineRepository orderLineRepository, ProductRepository productRepository) {
        this.orderLineRepository = orderLineRepository;
        this.productRepository = productRepository;
    }

    @Override
    public OrderLine save(OrderLine orderLine) {
        log.debug("Request to save OrderLine : {}", orderLine);
        return orderLineRepository.save(orderLine);
    }

    @Override
    @Transactional(readOnly = true)
    public List<OrderLine> findAll() {
        log.debug("Request to get all OrderLines");
        return orderLineRepository.findAll();
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<OrderLine> findOne(Long id) {
        log.debug("Request to get OrderLine : {}", id);
        return orderLineRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete OrderLine : {}", id);
        orderLineRepository.deleteById(id);
    }

	@Override
	public Optional<User> findUser(Long id) {
		return orderLineRepository.findUserOfOrderLine(id);
	}

	@Override
	public Optional<OrderLine> findOrderLineInCartByProductAndUser(Long productId, String login) {
		return Optional.ofNullable(orderLineRepository.findOrderLineOfCartByUserAndProduct(productId, login));
	}

    @Transactional
    public Long addToCart(OrderLine orderLine) throws OrderLineException {
        log.debug("Request to add OrderLine to cart : {}", orderLine);

        int quantity;

        // On fait ce select pour prendre le verrou sur l'orderLine tôt et prévenir un deadlock
        if (null != orderLine.getId()) {
            OrderLine oldOrderLine = orderLineRepository.findByIdLocked(orderLine.getId()).orElseThrow(() -> new OrderLineException("productnotincart"));
            quantity = orderLine.getQuantity() - oldOrderLine.getQuantity();
        } else {
            quantity = orderLine.getQuantity();
        }
        Product product = productRepository.findByIdLocked(orderLine.getProduct().getId()).orElseThrow(() -> new OrderLineException("productnotavailable"));

        if(product.getStockQuantity() - quantity < 0) {
            throw new OrderLineException("productnotavailable");
        }
        productRepository.addToStock(orderLine.getProduct().getId(),-quantity);

        if (null != orderLine.getId()) {
            orderLineRepository.addToCart(orderLine.getId(), quantity);
            return orderLine.getId();
        } else {
            return orderLineRepository.save(orderLine).getId();
        }
    }

    @Override
    public void removeFromCart(Long id) throws OrderLineException {
        OrderLine orderLine = orderLineRepository.findByIdLocked(id).orElseThrow(() -> new OrderLineException("productnotincart"));
        // On fait ce select pour prendre le verrou sur le produit tôt et prévenir un deadlock
        Product product = productRepository.findByIdLocked(orderLine.getProduct().getId()).orElseThrow(() -> new OrderLineException("productnotavailable"));

        //verification que l'orderline est bien dans le panier
        if (!orderLine.getProductOrder().getStatus().equals(OrderStatus.CART)) {
            throw new OrderLineException("productnotincart");
        }

        productRepository.addToStock(orderLine.getProduct().getId(),orderLine.getQuantity());
        orderLineRepository.deleteById(orderLine.getId());
    }

    @Override
    public List<OrderLine> findByOrderId(Long orderId) {
        log.debug("Request to find orderlines by order");
        return orderLineRepository.findByOrder(orderId);
    }
}

package com.ecomproject.spaghettomatos.service.impl;

import com.ecomproject.spaghettomatos.service.CustomerService;
import com.ecomproject.spaghettomatos.domain.Customer;
import com.ecomproject.spaghettomatos.domain.User;
import com.ecomproject.spaghettomatos.repository.CustomerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Customer}.
 */
@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

    private final Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);

    private final CustomerRepository customerRepository;

    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public Customer save(Customer customer) {
        log.debug("Request to save Customer : {}", customer);
        return customerRepository.save(customer);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Customer> findAll(Pageable pageable) {
        log.debug("Request to get all Customers");
        return customerRepository.findAll(pageable);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<Customer> findOne(Long id) {
        log.debug("Request to get Customer : {}", id);
        return customerRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Customer : {}", id);
        customerRepository.deleteById(id);
    }

	@Override
	public Optional<User> findUser(Long id) {
		return customerRepository.findUserOfCustomer(id);
	}


    public Customer registerCustomer(User user, String userBirthday) {
        Customer newCustomer = new Customer();
        newCustomer.setUser(user);
        newCustomer.setBirthDate(Instant.parse(userBirthday));
        customerRepository.save(newCustomer);
        return newCustomer;

    }

	@Override
	public Optional<Customer> findCustomer(String login) {
		return customerRepository.findCustomerOfUser(login);
	}

}

package com.ecomproject.spaghettomatos.service.impl;

import com.ecomproject.spaghettomatos.service.ProductOrderService;
import com.ecomproject.spaghettomatos.domain.ProductOrder;
import com.ecomproject.spaghettomatos.domain.User;
import com.ecomproject.spaghettomatos.repository.ProductOrderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link ProductOrder}.
 */
@Service
@Transactional
public class ProductOrderServiceImpl implements ProductOrderService {

    private final Logger log = LoggerFactory.getLogger(ProductOrderServiceImpl.class);

    private final ProductOrderRepository productOrderRepository;

    public ProductOrderServiceImpl(ProductOrderRepository productOrderRepository) {
        this.productOrderRepository = productOrderRepository;
    }

    @Override
    public ProductOrder save(ProductOrder productOrder) {
        log.debug("Request to save ProductOrder : {}", productOrder);
        return productOrderRepository.save(productOrder);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProductOrder> findAll() {
        log.debug("Request to get all ProductOrders");
        return productOrderRepository.findAll();
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<ProductOrder> findOne(Long id) {
        log.debug("Request to get ProductOrder : {}", id);
        return productOrderRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProductOrder : {}", id);
        productOrderRepository.deleteById(id);
    }

	@Override
	public Optional<User> findUser(Long id) {
		return productOrderRepository.findUserOfProductOrder(id);
	}

	@Override
	public Optional<ProductOrder> getUserCart(String login) {
		return productOrderRepository.findCart(login);
	}

    @Override
    public List<ProductOrder> getOrderHistory(String login) {
        return productOrderRepository.findOrderHistory(login);
    }

    @Override
    public Optional<ProductOrder> getCart(String login) {
        log.debug("Request to get user cart");
        return productOrderRepository.getUserCart(login);
    }
}

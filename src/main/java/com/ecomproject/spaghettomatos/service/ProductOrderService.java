package com.ecomproject.spaghettomatos.service;

import com.ecomproject.spaghettomatos.domain.ProductOrder;
import com.ecomproject.spaghettomatos.domain.User;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link ProductOrder}.
 */
public interface ProductOrderService {

    /**
     * Save a productOrder.
     *
     * @param productOrder the entity to save.
     * @return the persisted entity.
     */
    ProductOrder save(ProductOrder productOrder);

    /**
     * Get all the productOrders.
     *
     * @return the list of entities.
     */
    List<ProductOrder> findAll();


    /**
     * Get the "id" productOrder.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProductOrder> findOne(Long id);

    /**
     * Delete the "id" productOrder.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Find the user of the productOrder
     *
     * @param id the id of the entity.
     * @return User the user of the productOrder
     */
	Optional<User> findUser(Long id);

    /**
     * Find the productOrder for the user
     *
     *
     * @param userLogin the login of the user
     * @return ProductOrder the cart
     */
	Optional<ProductOrder> getUserCart(String userLogin);

    /**
     * Find the list of the user's completed orders
     *
     * @author Louka Soret
     * @param Login the login of the user
     * @return the list of the user's completed orders
     */
    List<ProductOrder> getOrderHistory(String Login);

    /**
     * Get the cart of the user
     *
     * @return List OrderLine The list of orderlines in the cart of the user
     */
    Optional<ProductOrder> getCart(String login);
}

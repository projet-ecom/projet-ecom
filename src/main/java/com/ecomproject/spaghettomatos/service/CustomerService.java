package com.ecomproject.spaghettomatos.service;

import com.ecomproject.spaghettomatos.domain.Customer;
import com.ecomproject.spaghettomatos.domain.User;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link Customer}.
 */
public interface CustomerService {

    /**
     * Save a customer.
     *
     * @param customer the entity to save.
     * @return the persisted entity.
     */
    Customer save(Customer customer);

    /**
     * Get all the customers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Customer> findAll(Pageable pageable);


    /**
     * Get the "id" customer.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Customer> findOne(Long id);

    /**
     * Delete the "id" customer.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Find the user of the customer
     *
     * @param id the id of the entity.
     * @return User the user of the customer
     */
    Optional<User> findUser(Long id);

    /**
     * Find the customer of the user
     *
     * @param login the login of the customer
     * @return Customer the customer of the user
     */
    Optional<Customer> findCustomer(String login);
}

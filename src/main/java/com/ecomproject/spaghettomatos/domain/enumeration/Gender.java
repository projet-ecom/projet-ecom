package com.ecomproject.spaghettomatos.domain.enumeration;

/**
 * The Gender enumeration.
 */
public enum Gender {
    MALE, FEMALE, OTHER
}

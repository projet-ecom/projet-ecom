package com.ecomproject.spaghettomatos.domain.enumeration;

/**
 * The OrderStatus enumeration.
 */
public enum OrderStatus {
    CART, COMPLETED, CANCELLED
}

package com.ecomproject.spaghettomatos.repository;

import com.ecomproject.spaghettomatos.domain.OrderLine;
import com.ecomproject.spaghettomatos.domain.Product;
import com.ecomproject.spaghettomatos.domain.User;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the OrderLine entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrderLineRepository extends JpaRepository<OrderLine, Long> {

	@Query("select user from User user, Customer customer, ProductOrder productorder, OrderLine orderline where customer.user = user and productorder.customer = customer and orderline.productOrder = productorder and orderline.id=:id")
	Optional<User> findUserOfOrderLine(@Param("id") Long id);

	@Query("select orderline from User user, Customer customer, ProductOrder productorder, OrderLine orderline, Product product where customer.user = user and productorder.customer = customer and orderline.productOrder = productorder and orderline.product = product and productorder.status = 'CART' and product.id=:id and user.login=:login")
	OrderLine findOrderLineOfCartByUserAndProduct(@Param("id") Long productId, @Param("login") String login);

    @Modifying
    @Query("update OrderLine ol set ol.quantity = ol.quantity + :quantity where ol.id = :id")
    Integer addToCart(@Param("id") Long id, @Param("quantity") Integer quantity);

    @Query("select orderline from OrderLine orderline where orderline.productOrder.id = :orderId")
	List<OrderLine> findByOrder(@Param("orderId") Long orderId);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("select orderline from OrderLine orderline where orderline.id = :id")
    Optional<OrderLine> findByIdLocked(@Param("id") Long id);
}

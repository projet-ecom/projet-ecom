package com.ecomproject.spaghettomatos.repository;

import com.ecomproject.spaghettomatos.domain.Category;

import com.ecomproject.spaghettomatos.domain.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Category entity.
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    @Query(value = "select distinct category from Category category left join fetch category.products",
        countQuery = "select count(distinct category) from Category category")
    Page<Category> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct category from Category category left join fetch category.products")
    List<Category> findAllWithEagerRelationships();

    @Query("select category from Category category left join fetch category.products where category.id =:id")
    Optional<Category> findOneWithEagerRelationships(@Param("id") Long id);

    /*@Query("select distinct category from Category category join category.products product where product.id =:id")
    List<Category> findAllCategoriesProduct(@Param("id") Long id);*/

    @Query("select distinct category from Category category join category.products product where category.parent is null and product.id =:id")
    Optional<Category> findTypeMaterial(@Param("id") Long id);


    @Query("select distinct category from Category category where category.parent is null")
    List<Category> getMaterial();


    @Query("select distinct category.products from Category category where category.id =:id")
    List<Product> findAllCategoriesFilter(@Param("id") Long id);

    @Query("select product from Product product where product.id =:id")
    Optional<Product> findOneProduct(@Param("id") Long id);
}

package com.ecomproject.spaghettomatos.repository;

import com.ecomproject.spaghettomatos.domain.OrderLine;
import com.ecomproject.spaghettomatos.domain.ProductOrder;
import com.ecomproject.spaghettomatos.domain.User;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ProductOrder entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductOrderRepository extends JpaRepository<ProductOrder, Long> {

    @Query("select user from User user, Customer customer, ProductOrder productorder where customer.user = user and productorder.customer = customer and productorder.id=:id")
	Optional<User> findUserOfProductOrder(@Param("id") Long id);

    /**
     * @author Louka Soret
     * @param login Current logged in user
     * @return the cart of login
     */
    @Query("select productorder from User user, Customer customer, ProductOrder productorder where customer.user = user and productorder.customer = customer and productorder.status='CART' and user.login=:login")
	Optional<ProductOrder> findCart(@Param("login") String login);

    /**
     * @author Louka Soret
     * @param login Current logged in user
     * @return the completed orders of login
     */
    @Query("select productorder from User user, Customer customer, ProductOrder productorder where customer.user = user and productorder.customer = customer and productorder.status='COMPLETED' and user.login=:login")
    List<ProductOrder> findOrderHistory(@Param("login") String login);

    /**
     * Return the cart of the current user
     *
     * @author Louka Soret
     * @param login Current logged in user
     * @return the cart of the user
     */
    @Query("select productorder from User user, Customer customer, ProductOrder productorder where customer.user = user and productorder.customer = customer and productorder.status='CART' and user.login=:login")
    Optional<ProductOrder> getUserCart(@Param("login") String login);
}

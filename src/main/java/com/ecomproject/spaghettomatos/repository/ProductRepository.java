package com.ecomproject.spaghettomatos.repository;

import com.ecomproject.spaghettomatos.domain.Category;
import com.ecomproject.spaghettomatos.domain.Product;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.List;
import java.util.Optional;
/**
 * Spring Data  repository for the Product entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    @Modifying
    @Query("update Product p set p.stockQuantity = p.stockQuantity + :quantity where p.id = :id")
    Integer addToStock(@Param("id") Long id, @Param("quantity") Integer quantity);

    @Query("select product from Product product left join fetch product.categories where product.id =:id")
    Optional<Product> findOneProductWithCategories(@Param("id") Long id);

    @Query("select category from Category category where category.id =:id")
    Optional<Category> getOneCategory(@Param("id") Long id);

    @Query("select category from Category category where category.id =:id")
    Optional<Category> getAllCategories(@Param("id") Long id);

    @Query("select distinct category from Category category join category.products product where product.id =:id")
    List<Category> findAllCategoriesWithIdProduct(@Param("id") Long id);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("select product from Product product where product.id = :id")
    Optional<Product> findByIdLocked(@Param("id") Long id);
}

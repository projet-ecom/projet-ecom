package com.ecomproject.spaghettomatos.repository;

import com.ecomproject.spaghettomatos.domain.Category;
import com.ecomproject.spaghettomatos.domain.Customer;
import com.ecomproject.spaghettomatos.domain.User;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Customer entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    @Query("select user from User user, Customer customer where customer.user = user and customer.id =:id")
    Optional<User> findUserOfCustomer(@Param("id") long id);

    @Query("select customer from User user, Customer customer where customer.user = user and user.login =:login")
    Optional<Customer> findCustomerOfUser(@Param("login") String login);

}

package com.ecomproject.spaghettomatos.web.rest;

import com.ecomproject.spaghettomatos.domain.Category;
import com.ecomproject.spaghettomatos.domain.Product;
import com.ecomproject.spaghettomatos.service.CategoryService;
import com.ecomproject.spaghettomatos.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.ecomproject.spaghettomatos.domain.Category}.
 */
@RestController
@RequestMapping("/api")
public class CategoryResource {

    private final Logger log = LoggerFactory.getLogger(CategoryResource.class);

    private static final String ENTITY_NAME = "category";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CategoryService categoryService;

    public CategoryResource(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    /**
     * {@code POST  /categories} : Create a new category.
     *
     * @param category the category to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new category, or with status {@code 400 (Bad Request)} if the category has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/categories")
    @PreAuthorize(value = "hasRole('ADMIN')")
    public ResponseEntity<Category> createCategory(@RequestBody Category category) throws URISyntaxException {
        log.debug("REST request to save Category : {}", category);
        if (category.getId() != null) {
            throw new BadRequestAlertException("A new category cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Category result = categoryService.save(category);
        return ResponseEntity.created(new URI("/api/categories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /categories} : Updates an existing category.
     *
     * @param category the category to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated category,
     * or with status {@code 400 (Bad Request)} if the category is not valid,
     * or with status {@code 500 (Internal Server Error)} if the category couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/categories")
    @PreAuthorize(value = "hasRole('ADMIN')")
    public ResponseEntity<Category> updateCategory(@RequestBody Category category) throws URISyntaxException {
        log.debug("REST request to update Category : {}", category);
        if (category.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Category result = categoryService.save(category);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, category.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /categories} : get all the categories.
     *
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of categories in body.
     */
    @GetMapping("/categories")
    public List<Category> getAllCategories(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get all Categories");
        return categoryService.findAll();
    }

    /**
     * {@code GET  /categories/:id} : get the "id" category.
     *
     * @param id the id of the category to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the category, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/categories/{id}")
    public ResponseEntity<Category> getCategory(@PathVariable Long id) {
        log.debug("REST request to get Category : {}", id);
        Optional<Category> category = categoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(category);
    }

    /**
     * {@code GET /categories/treeCategory} : get all categories in a tree
     *
     * @return Category a category with all other categories as direct or indirect children
     */
    @GetMapping("/categories/treeCategory")
    public Category getTreeCategory() {
        log.debug("REST request to get tree Category");
        Category category = categoryService.getTreeCategory();
        return category;
    }

    /**
     * {@code GET  /categories/byProduct/:id} : get the "id" category.
     *
     * @param idProduct the id of the product to retrieve.
     * @return Category
     */
    @GetMapping("/categories/byProduct/{idProduct}")
    public Category getAllCategoriesWithProduct(@PathVariable Long idProduct) {
        log.debug("REST request to get Category with product {}", idProduct);
        return categoryService.findAllCategoriesWithProduct(idProduct);
    }

    /**
     * {@code GET /categories/filter/:listId} : get the products that are direct or indirect childs of the given categories
     *
     * @param listId list of categories id
     * @return List<Product>
     */
    @GetMapping("/categories/filter/{listId}")
    public ResponseEntity<List<Product>> findAllCategoriesFilter(@PathVariable String listId, Pageable pageable){

        String[] list_idString = listId.split(",");
        Long[] list_id = new Long[list_idString.length];
        for (int i=0; i<list_idString.length; i++){
            list_id[i]=Long.parseLong(list_idString[i]);
        }

        Page<Product> page = categoryService.findAllCategoriesFilter(list_id,pageable);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /categories/findChildren/:id} : get the "id" category.
     *
     * @param id the id of the product to retrieve.
     * @return Category[]
     */
    @GetMapping("/categories/findChildren/{id}")
    public Category[] getChildrenWithCategories(@PathVariable Long id) {
        log.debug("REST request to get hildrenWithCategories with product {}", id);
        return categoryService.getChildrenWithCategories(id);
    }

    /**
     * {@code GET  /categories/findMaterial} : return all categories with no parents
     *
     * @param
     * @return Category
     */
    @GetMapping("/categories/findMaterial")
    public Category getMaterial() {
        log.debug("REST request to get material");
        return categoryService.getMaterial();
    }

    /**
     * {@code DELETE  /categories/:id} : delete the "id" category.
     *
     * @param id the id of the category to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/categories/{id}")
    @PreAuthorize(value = "hasRole('ADMIN')")
    public ResponseEntity<Void> deleteCategory(@PathVariable Long id) {
        log.debug("REST request to delete Category : {}", id);
        categoryService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

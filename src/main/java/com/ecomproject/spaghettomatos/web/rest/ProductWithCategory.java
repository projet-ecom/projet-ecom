package com.ecomproject.spaghettomatos.web.rest;

import com.ecomproject.spaghettomatos.domain.Category;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

public class ProductWithCategory {
    Long id;
    String name;
    Integer stockQuantity;
    BigDecimal price;
    String description;
    String imagePath;
    Set<Category> categories = new HashSet<>();

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getStockQuantity() {
        return stockQuantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public String getImagePath() {
        return imagePath;
    }

    public Set<Category> getCategories() {
        return categories;
    }
}

package com.ecomproject.spaghettomatos.web.rest;

import com.ecomproject.spaghettomatos.domain.OrderLine;
import com.ecomproject.spaghettomatos.domain.ProductOrder;
import com.ecomproject.spaghettomatos.domain.enumeration.OrderStatus;
import com.ecomproject.spaghettomatos.security.SecurityUtils;
import com.ecomproject.spaghettomatos.service.CustomerService;
import com.ecomproject.spaghettomatos.service.OrderLineService;
import com.ecomproject.spaghettomatos.service.ProductOrderService;
import com.ecomproject.spaghettomatos.web.rest.errors.BadRequestAlertException;

import com.ecomproject.spaghettomatos.service.errors.OrderLineException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.ecomproject.spaghettomatos.domain.OrderLine}.
 */
@RestController
@RequestMapping("/api")
public class OrderLineResource {

    private static class OrderLineResourceException extends RuntimeException {
        private OrderLineResourceException(String message) {
            super(message);
        }
    }

    private final Logger log = LoggerFactory.getLogger(OrderLineResource.class);

    private static final String ENTITY_NAME = "orderLine";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OrderLineService orderLineService;
    private final ProductOrderService productOrderService;
    private final CustomerService customerService;


    public OrderLineResource(OrderLineService orderLineService,ProductOrderService productOrderService, CustomerService customerService) {
        this.customerService = customerService;
		this.productOrderService = productOrderService;
		this.orderLineService = orderLineService;
    }

    /**
     * {@code POST  /order-lines} : Create a new orderLine.
     *
     * @param orderLine the orderLine to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new orderLine, or with status {@code 400 (Bad Request)} if the orderLine has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/order-lines")
    @PreAuthorize(value = "hasRole('ADMIN')"+ " or @userSecurity.isOrderLineOfAuthentificatedUser(authentication, #orderLine.id) ")
    public ResponseEntity<OrderLine> createOrderLine(@Valid @RequestBody OrderLine orderLine) throws URISyntaxException {
        log.debug("REST request to save OrderLine : {}", orderLine);
        if (orderLine.getId() != null) {
            throw new BadRequestAlertException("A new orderLine cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OrderLine result = orderLineService.save(orderLine);
        return ResponseEntity.created(new URI("/api/order-lines/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /order-lines} : Updates an existing orderLine.
     *
     * @param orderLine the orderLine to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated orderLine,
     * or with status {@code 400 (Bad Request)} if the orderLine is not valid,
     * or with status {@code 500 (Internal Server Error)} if the orderLine couldn't be updated.
     */
    @PutMapping("/order-lines")
    @PreAuthorize(value = "hasRole('ADMIN')"+ " or @userSecurity.isOrderLineOfAuthentificatedUser(authentication, #orderLine.id) ")
    public ResponseEntity<OrderLine> updateOrderLine(@Valid @RequestBody OrderLine orderLine) {
        log.debug("REST request to update OrderLine : {}", orderLine);
        if (orderLine.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OrderLine result = orderLineService.save(orderLine);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, orderLine.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /order-lines} : get all the orderLines.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of orderLines in body.
     */
    @GetMapping("/order-lines")
    @PreAuthorize(value = "hasRole('ADMIN')")
    public List<OrderLine> getAllOrderLines() {
        log.debug("REST request to get all OrderLines");
        return orderLineService.findAll();
    }

    /**
     * {@code GET  /order-lines/:id} : get the "id" orderLine.
     *
     * @param id the id of the orderLine to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the orderLine, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/order-lines/{id}")
    @PreAuthorize(value = "hasRole('ADMIN')"+ " or @userSecurity.isOrderLineOfAuthentificatedUser(authentication, #id) ")
    public ResponseEntity<OrderLine> getOrderLine(@PathVariable Long id) {
        log.debug("REST request to get OrderLine : {}", id);
        Optional<OrderLine> orderLine = orderLineService.findOne(id);
        return ResponseUtil.wrapOrNotFound(orderLine);
    }

    /**
     * {@code DELETE  /order-lines/:id} : delete the "id" orderLine.
     *
     * @param id the id of the orderLine to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/order-lines/{id}")
    @PreAuthorize(value = "hasRole('ADMIN')"+ " or @userSecurity.isOrderLineOfAuthentificatedUser(authentication, #id) ")
    public ResponseEntity<Void> deleteOrderLine(@PathVariable Long id) {
        log.debug("REST request to delete OrderLine : {}", id);
        orderLineService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }


    /**
     * {@code POST  /cart} : Create a new orderLine linked to the cart of the authentificated user.
     *
     * @param orderLine the orderLine to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new orderLine, or with status {@code 400 (Bad Request)} if the orderLine has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/cart")
    @PreAuthorize(value = "hasRole('USER')")
    public ResponseEntity<OrderLine> createOrderLineInCart(@Valid @RequestBody OrderLine orderLine) throws URISyntaxException {
        log.debug("REST request to save OrderLine : {}", orderLine);
        if (orderLine.getId() != null) {
            throw new BadRequestAlertException("A new orderLine cannot already have an ID", ENTITY_NAME, "idexists");
        }

        // changement de l'attribut productorder du panier
    	String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new OrderLineResourceException("Current user login not found"));

		Optional<ProductOrder> po = productOrderService.getUserCart(userLogin);
		if(!po.isPresent()) {
			ProductOrder cart = new ProductOrder();
			cart.setStatus(OrderStatus.CART);
			cart.setCustomer(customerService.findCustomer(userLogin).orElseThrow(() -> new OrderLineResourceException("Customer not found")));
			cart.setCreationDate(Instant.now());
			cart = productOrderService.save(cart);
			orderLine.setProductOrder(cart);
		} else {
    		orderLine.setProductOrder(po.get());
		}
        Optional<OrderLine> ol = orderLineService.findOrderLineInCartByProductAndUser(orderLine.getProduct().getId(), userLogin);
    	if (ol.isPresent()) {
			throw new BadRequestAlertException("product already in cart", ENTITY_NAME, "productincart");
		}

        OrderLine result;
        try {
            result = orderLineService.findOne(orderLineService.addToCart(orderLine)).get();
        } catch (OrderLineException e) {
            throw new BadRequestAlertException("", ENTITY_NAME, e.getMessage());
        }
        return ResponseEntity.created(new URI("/api/order-lines/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /cart} : modify a new orderLine linked to the cart of the authentificated user.
     *
     * @param orderLine the orderLine to create.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated orderLine,
     * or with status {@code 400 (Bad Request)} if the orderLine is not valid,
     * or with status {@code 500 (Internal Server Error)} if the orderLine couldn't be updated.
     */
    @PutMapping("/cart")
    @PreAuthorize(value = "hasRole('ADMIN')"+ " or hasRole('USER')")
    public ResponseEntity<OrderLine> updateOrderLineInCart(@Valid @RequestBody OrderLine orderLine) {
        log.debug("REST request to update OrderLine : {}", orderLine);
        if (orderLine.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

		if (!orderLine.getProductOrder().getStatus().equals(OrderStatus.CART)) {
			throw new BadRequestAlertException("Invalid Status of ProductOrder", ENTITY_NAME, "invalidstatus");
		}

		// on vérifie que l'utilisateur ne retire pas une ligne à un ordre complété ou annulé
		Optional<OrderLine> ol = orderLineService.findOne(orderLine.getId());
		if(!ol.get().getProductOrder().getStatus().equals(OrderStatus.CART)) {
			throw new BadRequestAlertException("Invalid Status of ProductOrder", ENTITY_NAME, "invalidstatus");
		}

    	String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new OrderLineResourceException("Current user login not found"));

        Optional<OrderLine> olInCart = orderLineService.findOrderLineInCartByProductAndUser(orderLine.getProduct().getId(), userLogin);
    	if (!olInCart.isPresent()) {
			throw new BadRequestAlertException("product is not in cart", ENTITY_NAME, "productnotincart");
		}
    	// on verifie que l'id de l'orderline modifiée est identique à l'ancienne pour le même produit
    	else if(!olInCart.get().getId().equals(orderLine.getId())){
    		throw new BadRequestAlertException("Invalid product", ENTITY_NAME, "invalidproduct");
    	}

        OrderLine result;
        try {
            result = orderLineService.findOne(orderLineService.addToCart(orderLine)).get();
        } catch (OrderLineException e) {
            throw new BadRequestAlertException("", ENTITY_NAME, e.getMessage());
        }
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, orderLine.getId().toString()))
            .body(result);
    }

    /**
    * @code GET  /cart/lineofproduct/:id} : get the productid orderLine in the cart of the authentificated user.
    * @param id the id of the product.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the orderLine, or with status {@code 404 (Not Found)}.
    */
    @GetMapping("/cart/lineofproduct/{id}")
    @PreAuthorize(value = "hasRole('ADMIN')"+ " or hasRole('USER')")
    public OrderLine getOrderLineOfProduct(@PathVariable Long id) {
        String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new OrderLineResourceException("Unable to find cart: user login not found"));
		return orderLineService.findOrderLineInCartByProductAndUser(id, userLogin).orElse(null);
    }

    /**
     * {@code DELETE  /cart/:id} : delete the "id" orderLine.
     *
     * @param id the id of the orderLine to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/cart/{id}")
    @PreAuthorize(value = "hasRole('ADMIN')" + " or @userSecurity.isOrderLineOfAuthentificatedUser(authentication, #id) ")
    public ResponseEntity<Void> deleteFromCart(@PathVariable Long id) {
        log.debug("REST request to delete OrderLine : {}", id);
        try {
            orderLineService.removeFromCart(id);
        } catch (OrderLineException e) {
            throw new BadRequestAlertException("", ENTITY_NAME, e.getMessage());
        }
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

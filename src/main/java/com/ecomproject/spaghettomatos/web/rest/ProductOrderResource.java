package com.ecomproject.spaghettomatos.web.rest;

import com.ecomproject.spaghettomatos.domain.OrderLine;
import com.ecomproject.spaghettomatos.domain.ProductOrder;
import com.ecomproject.spaghettomatos.domain.enumeration.OrderStatus;
import com.ecomproject.spaghettomatos.security.SecurityUtils;
import com.ecomproject.spaghettomatos.service.OrderLineService;
import com.ecomproject.spaghettomatos.service.ProductOrderService;
import com.ecomproject.spaghettomatos.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * REST controller for managing {@link com.ecomproject.spaghettomatos.domain.ProductOrder}.
 */
@RestController
@RequestMapping("/api")
public class ProductOrderResource {

    private static class ProductOrderResourceException extends RuntimeException {
        private ProductOrderResourceException(String message) {
            super(message);
        }
    }

    private final Logger log = LoggerFactory.getLogger(ProductOrderResource.class);

    private static final String ENTITY_NAME = "productOrder";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProductOrderService productOrderService;
    private final OrderLineService orderLinesService;

    public ProductOrderResource(ProductOrderService productOrderService, OrderLineService orderLinesService) {
        this.productOrderService = productOrderService;
        this.orderLinesService = orderLinesService;
    }

    /**
     * {@code POST  /product-orders} : Create a new productOrder.
     *
     * @param productOrder the productOrder to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new productOrder, or with status {@code 400 (Bad Request)} if the productOrder has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/product-orders")
    @PreAuthorize(value = "hasRole('ADMIN')")
    public ResponseEntity<ProductOrder> createProductOrder(@Valid @RequestBody ProductOrder productOrder) throws URISyntaxException {
        log.debug("REST request to save ProductOrder : {}", productOrder);
        if (productOrder.getId() != null) {
            throw new BadRequestAlertException("A new productOrder cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProductOrder result = productOrderService.save(productOrder);
        return ResponseEntity.created(new URI("/api/product-orders/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /product-orders} : Updates an existing productOrder.
     *
     * @param productOrder the productOrder to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated productOrder,
     * or with status {@code 400 (Bad Request)} if the productOrder is not valid,
     * or with status {@code 500 (Internal Server Error)} if the productOrder couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/product-orders")
    @PreAuthorize(value = "hasRole('ADMIN')"+ " or @userSecurity.isProductOrderOfAuthentificatedUser(authentication, #productOrder.id) ")
    public ResponseEntity<ProductOrder> updateProductOrder(@Valid @RequestBody ProductOrder productOrder) throws URISyntaxException {
        log.debug("REST request to update ProductOrder : {}", productOrder);
        if (productOrder.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProductOrder result = productOrderService.save(productOrder);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, productOrder.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /product-orders} : get all the productOrders.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of productOrders in body.
     */
    @GetMapping("/product-orders")
    @PreAuthorize(value = "hasRole('ADMIN')")
    public List<ProductOrder> getAllProductOrders() {
        log.debug("REST request to get all ProductOrders");
        return productOrderService.findAll();
    }

    /**
     * {@code GET  /product-orders/:id} : get the "id" productOrder.
     *
     * @param id the id of the productOrder to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the productOrder, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/product-orders/{id}")
    @PreAuthorize(value = "hasRole('ADMIN')"+ " or @userSecurity.isProductOrderOfAuthentificatedUser(authentication, #id) ")
    public ResponseEntity<ProductOrder> getProductOrder(@PathVariable Long id) {
        log.debug("REST request to get ProductOrder : {}", id);
        Optional<ProductOrder> productOrder = productOrderService.findOne(id);
        if(productOrder.isPresent()){ // Get the orderlines of the order
            productOrder.get().orderLines(new HashSet<>(orderLinesService.findByOrderId(productOrder.get().getId())));
        }
        return ResponseUtil.wrapOrNotFound(productOrder);
    }

    /**
     * {@code DELETE  /product-orders/:id} : delete the "id" productOrder.
     *
     * @param id the id of the productOrder to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/product-orders/{id}")
    @PreAuthorize(value = "hasRole('ADMIN')")
    public ResponseEntity<Void> deleteProductOrder(@PathVariable Long id) {
        log.debug("REST request to delete ProductOrder : {}", id);
        productOrderService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/order-history")
    @PreAuthorize(value = "hasRole('USER')")
    public List<ProductOrder> getOrderHistory() throws Exception {
        log.debug("REST request to get user order history");
        String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new Exception("Unable to find cart: user login not found"));
        List<ProductOrder> productOrders = productOrderService.getOrderHistory(userLogin);
        for (ProductOrder po : productOrders) {
            po.orderLines(new HashSet<>(orderLinesService.findByOrderId(po.getId())));
        }

        return productOrders;
    }

    /**
     * @author Louka Soret
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of orderLines in body.
     */
    @GetMapping("/cart")
    @PreAuthorize(value = "hasRole('USER')")
    public Optional<ProductOrder> getCart(){
        log.debug("REST request to get user cart");
        String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new ProductOrderResource.ProductOrderResourceException("Unable to find cart: user login not found"));
        Optional<ProductOrder> productOrder = productOrderService.getCart(userLogin);
        if(productOrder.isPresent()){ // Get the orderlines of the order
            productOrder.get().orderLines(new HashSet<>(orderLinesService.findByOrderId(productOrder.get().getId())));
        }

        return productOrder;
    }

    /**
     * {@code PUT  /cart/confirm} : Confirm the cart of the user.
     *
     * @author Louka Soret
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated productOrder,
     * or with status {@code 400 (Bad Request)} if the productOrder is not valid,
     * or with status {@code 500 (Internal Server Error)} if the productOrder couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/cart/confirm")
    @PreAuthorize(value = "@userSecurity.isProductOrderOfAuthentificatedUser(authentication, #productOrder.getId())")
    public ResponseEntity<ProductOrder> confirmCart(@Valid @RequestBody ProductOrder productOrder) throws URISyntaxException {
        log.debug("REST request to confirm an order");
        Optional<ProductOrder> cart = getCart();
        if (!cart.isPresent()) {
            throw new BadRequestAlertException("Cart not found ", ENTITY_NAME, "not found");
        }
        cart.get().setStatus(OrderStatus.COMPLETED);
        cart.get().setCreationDate(Instant.now());
        ProductOrder result = productOrderService.save(cart.get());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, cart.get().getId().toString()))
            .body(result);
    }
}

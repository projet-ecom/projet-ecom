package com.ecomproject.spaghettomatos.security;

import com.ecomproject.spaghettomatos.service.UserService;
import com.ecomproject.spaghettomatos.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import java.util.Optional;

import com.ecomproject.spaghettomatos.service.CustomerService;
import com.ecomproject.spaghettomatos.service.OrderLineService;
import com.ecomproject.spaghettomatos.service.ProductOrderService;

@Component("userSecurity")
public class UserSecurity {
	@Autowired
	CustomerService customerService;

	@Autowired
	ProductOrderService productOrderService;

	@Autowired
	OrderLineService orderLineService;

	@Autowired
    UserService userService;

     private final Logger log = LoggerFactory.getLogger(UserSecurity.class);

     public boolean isCustomerOfAuthentificatedUser(Authentication authentication, String customerId) {
         Optional<User> user = customerService.findUser(Long.parseLong(customerId));
         return user.map(value -> value.getLogin().equals(authentication.getName())).orElse(false);
     }

    public boolean isProductOrderOfAuthentificatedUser(Authentication authentication, String productOrderId) {
        Optional<User> user = productOrderService.findUser(Long.parseLong(productOrderId));
        return user.map(value -> value.getLogin().equals(authentication.getName())).orElse(false);
    }

    public boolean isOrderLineOfAuthentificatedUser(Authentication authentication, String orderLineId) {
        Optional<User> user = orderLineService.findUser(Long.parseLong(orderLineId));
        return user.map(value -> value.getLogin().equals(authentication.getName())).orElse(false);
    }

    public boolean isLoginOfAuthentificatedUser(Authentication authentication, String login) {
        return userService.getUserWithAuthoritiesByLogin(login).isPresent();
    }

}
